package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		fmt.Fprintf(w, "Handling GET request")
	} else if r.Method == http.MethodPost {
		fmt.Fprintf(w, "Handling POST request")
	} else {
		http.Error(w, "Invalid request method: ", http.StatusMethodNotAllowed)
	}
}

func main() {
	http.HandleFunc("/", handler)
	_ = http.ListenAndServe(":8080", nil)
}
