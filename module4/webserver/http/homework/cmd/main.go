package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main() {

	users := []User{User{
		ID:   1,
		Name: "Ivan",
		Age:  20,
	}, {
		ID:   2,
		Name: "Boris",
		Age:  30,
	}}

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		hello, _ := json.Marshal("Hello, my friend!")
		_, _ = w.Write(hello)
	})
	r.Get("/users", func(w http.ResponseWriter, r *http.Request) {
		showUsers, _ := json.Marshal(users)
		_, _ = w.Write(showUsers)
	})
	r.Get("/user/{id}", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")

		_, _ = w.Write([]byte(id))
	})
	_ = http.ListenAndServe(":3000", r)

}

//func getArticle(w http.ResponseWriter, r *http.Request) {

//if err != nil {
//	w.WriteHeader(422)
//	w.Write([]byte(fmt.Sprintf("error fetching article %s-%s: %v", dateParam, slugParam, err)))
//	return
//}
//
//if article == nil {
//	w.WriteHeader(404)
//	w.Write([]byte("article not found"))
//	return
//}
//w.Write([]byte(article.Text()))

//}

//
//func handleConnection(conn net.Conn) {
//	defer conn.Close()
//	reader := bufio.NewReader(conn)
//
//	// Read the request
//	request, _ := reader.ReadString('\n')
//	fmt.Println(request)
//
//	// Parse the request
//	parts := strings.Split(request, " ")
//	if len(parts) < 2 {
//		return
//	}
//	method := parts[0]
//	path := parts[1]
//
//	// Write the response
//	switch {
//	case method == "GET" && path == "/":
//		m := "HTTP/1.1 200 OK\n"
//		m = m + "Server: nginx/1.19.2\n"
//		m = m + "Date: Mon, 19 Oct 2020 13:13:29 GMT\n"
//		m = m + "Content-Type: text/html\n"
//		m = m + "Content-Length: 98\n" // указываем размер контента
//		m = m + "Last-Modified: Mon, 19 Oct 2020 13:13:13 GMT\n"
//		m = m + "Connection: keep-alive\n"
//		m = m + "ETag: \"5f8d90e9-62\"\n"
//		m = m + "Accept-Ranges: bytes\n"
//		m = m + "\n"
//		m = m + "<!DOCTYPE html>\n"
//		m = m + "<html>\n"
//		m = m + "<head>\n"
//		m = m + "<title>Webserver</title>\n"
//		m = m + "</head>\n"
//		m = m + "<body>\n"
//		m = m + "hello world\n"
//		m = m + "</body>\n"
//		m = m + "</html>\n"
//		_, _ = conn.Write([]byte(m))
//	case method == "POST" && path == "/upload":
//		// Read the file from the request body
//		file, _ := os.Create("uploaded_file.txt")
//		defer file.Close()
//		_, _ = io.Copy(file, reader)
//		fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nFile uploaded successfully!")
//	default:
//		fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
//
//	}
//
//}
//
//func main() {
//	listener, _ := net.Listen("tcp", ":8080")
//	defer listener.Close()
//
//	for {
//		conn, _ := listener.Accept()
//		go handleConnection(conn)
//	}
//}
