// Напишите функцию, которая находит минимальное и максимальное значения в заданном массиве int с помощью указателей.

package main

import "fmt"

func main() {
	m := [...]int{6, 2, 8, 4, 3}
	fmt.Println(m)
	minmax(&m)
	//fmt.Println(m)

}

func minmax(m *[5]int) {
	min := m[0]
	max := m[0]
	for _, v := range m {
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}
	fmt.Println(max, min)
}
