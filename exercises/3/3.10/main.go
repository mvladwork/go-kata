// Напишите функцию, которая принимает указатель на структуру Circle и возвращает указатель на ее площадь.
package main

import "fmt"

type Circle struct {
	l int
	r int
}

func (c Circle) area() int {
	return c.l * c.r
}

func main() {
	c := Circle{20, 5}
	fmt.Println(*fun(&c))

}

func fun(c *Circle) *int {
	s := c.area()
	return &s
}
