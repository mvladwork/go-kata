// Напишите функцию, которая меняет местами значения двух переменных типа int с помощью указателей.

package main

import "fmt"

func main() {

	v1 := 56
	v2 := 33
	fmt.Println(v1, v2)
	varChange(&v1, &v2)
	fmt.Println(v1, v2)
}

func varChange(v1 *int, v2 *int) {
	*v1, *v2 = *v2, *v1
}
