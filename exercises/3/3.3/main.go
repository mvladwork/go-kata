// Напишите функцию, которая принимает указатель на int и возвращает указатель на его квадрат.
package main

import "fmt"

func main() {
	v := 5
	q := fun(&v)
	fmt.Println(*q)
}

func fun(i *int) *int {
	q := *i * *i
	return &q
}
