// Напишите функцию, которая принимает указатель на структуру Person и изменяет ее поле Name.

package main

import "fmt"

type Person struct {
	Name string
}

func main() {
	p := Person{Name: "Ваня"}
	fmt.Println(p)
	fun(&p)
	fmt.Println(p)
}

func fun(f *Person) {
	f.Name = "Игорь"
}
