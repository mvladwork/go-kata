// Напишите функцию, которая принимает указатель на массив int и возвращает указатель на его первый элемент.

package main

import "fmt"

func main() {

	m := [3]int{45, 23, 77}
	fmt.Println(&m[1])
	d := fun(&m)
	fmt.Println(d)
}

func fun(m *[3]int) *int {
	return &m[1]
}
