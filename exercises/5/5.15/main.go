package main

import (
	"fmt"
	"sync"
)

type cash struct {
	d map[int]string
	//mu *sync.Mutex
}

func (c *cash) set(i int, s string) {
	//c.mu.Lock()
	//defer c.mu.Unlock()
	c.d[i] = s
}

func (c *cash) get(i int) string {
	//c.mu.Lock()
	//defer c.mu.Unlock()
	return c.d[i]
}

func main() {
	c := cash{
		d: make(map[int]string),
		//mu: &sync.Mutex{},
	}

	wg := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			c.set(i, fmt.Sprint("element ", i))
			fmt.Println(c.get(i))
			wg.Done()
		}(i)
	}
	wg.Wait()

}
