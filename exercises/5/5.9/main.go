package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Производитель-потребитель:
// Создайте две горутины, одна будет производителем (producer), а другая потребителем (consumer). Производитель будет
// генерировать случайные числа и отправлять их через канал потребителю. Потребитель будет выводить полученные числа
// на экран.
func main() {

	a := make(chan int)
	rand.Seed(time.Now().UnixNano())
	// producer
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for i := 0; i < 10; i++ {
			time.Sleep(500 * time.Millisecond)
			a <- rand.Intn(9) + 1
		}
		close(a)
	}()
	// consumer
	go func() {
		for v := range a {
			fmt.Println(v)
		}
		wg.Done()
	}()

	wg.Wait()
}
