package main

import (
	"fmt"
	"os"
	"strings"
	"sync"
)

// Напишите программу, которая считывает содержимое двух файлов и выводит на экран количество общих слов в этих
// файлах. Для считывания файлов используйте горутины. Для синхронизации доступа к общему словарю используйте мьютекс

func main() {
	//a := make(chan string)
	list := [2]string{"/tmp/list1", "/tmp/list2"}
	all := make([]string, 0)
	mu := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	for _, v := range list {
		wg.Add(1)
		go func(v string) {

			data, err := os.ReadFile(v)
			if err != nil {
				panic(err)
			}
			for _, v := range strings.Split(string(data), " ") {
				mu.Lock()
				all = append(all, v)
				mu.Unlock()
			}
			wg.Done()
		}(v)
	}

	wg.Wait()
	count := 0
	for _, v := range all {
		_ = v
		count++
	}
	fmt.Printf("Всего %v слов в двух словарях", count)
}
