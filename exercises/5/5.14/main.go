package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var v int64
	//mu := sync.Mutex{}
	wg := sync.WaitGroup{}
	for i := 1; i <= 1000; i++ {
		wg.Add(1)
		go func(i int) {
			//mu.Lock()
			atomic.AddInt64(&v, 1)
			//v++
			//mu.Unlock()
			wg.Done()
		}(i)
	}

	wg.Wait()
	fmt.Println(v)
}
