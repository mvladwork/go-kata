package main

import (
	"fmt"
	"sort"
	"sync"
)

// Написать программу, которая будет создавать 2 горутины и каждая из них будет выполнять функцию,
// которая будет складывать два числа. Одна горутина будет складывать 10 и 20, а другая - 30 и 40.
// Результаты сложения нужно выводить на экран. Но результаты должны выводиться в порядке возрастания.
// Используйте мьютекс для синхронизации.

func main() {
	mu := sync.Mutex{}
	wg := sync.WaitGroup{}
	wg.Add(2)
	res := make([]int, 0)
	go func() {
		t := 10 + 20
		mu.Lock()
		res = append(res, t)
		mu.Unlock()
		wg.Done()
	}()
	go func() {
		t := 30 + 40
		mu.Lock()
		res = append(res, t)
		mu.Unlock()
		wg.Done()
	}()
	wg.Wait()
	sort.Ints(res)
	for _, v := range res {
		fmt.Println(v)
	}
}
