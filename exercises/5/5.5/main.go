package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Напишите программу, которая создает 10 горутин, каждая из которых генерирует случайное число и записывает его
// в канал. После того, как все горутины закончат свою работу, программа должна считать сумму всех чисел из канала.
// Используйте WaitGroup для ожидания завершения работы горутин

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int, 10)
	wg := sync.WaitGroup{}
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			a <- rand.Intn(10) + 1
		}()
	}
	wg.Wait()
	close(a)

	sum := 0
	for v := range a {
		sum += v
	}
	fmt.Println(sum)
}
