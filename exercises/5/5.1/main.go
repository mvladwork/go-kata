// 5.1 Написать программу, которая будет создавать 10 горутин и каждая из них будет выводить на экран свой номер
//в бесконечном цикле. Но номера должны выводиться в порядке возрастания. Используйте мьютекс для синхронизации.

package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	mu := sync.Mutex{}
	for i := 1; i <= 10; i++ {

		go func(i int) {
			i2 := 0
			for {
				mu.Lock()
				i2++
				fmt.Printf("i%v = %v\n", i, i2)
				mu.Unlock()
				if i2 >= 10 {
					return
				}

			}
		}(i)
	}

	time.Sleep(100 * time.Millisecond)
}
