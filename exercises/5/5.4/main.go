package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Напишите программу, которая запускает три горутины. Первая горутина записывает числа в канал, вторая горутина
// считывает числа из канала и возводит их в квадрат, а третья горутина выводит квадраты чисел на экран.
// Используйте мьютекс для синхронизации доступа к каналу.

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	mu := &sync.Mutex{}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		defer close(a)
		for i := 1; i <= 5; i++ {
			mu.Lock()
			a <- rand.Intn(10) + 1
			mu.Unlock()
		}
		wg.Done()
	}()

	go func() {
		for c := range a {
			b <- c * c
		}
		close(b)
		wg.Done()
	}()

	go func() {
		for c := range b {
			fmt.Println(c)
		}
		wg.Done()
	}()

	wg.Wait()

}
