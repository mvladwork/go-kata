package main

import "fmt"

//Параллельная сортировка массива:
//Реализуйте параллельную сортировку массива с помощью алгоритма сортировки слиянием. Разделите массив на две равные
//части, и запустите две горутины для сортировки каждой из частей. Затем объедините отсортированные части с помощью
//еще одной горутины.

func sort(s []int) []int {
	if len(s) == 1 {
		return s
	}
	mid := len(s) / 2
	left := sort(s[:mid])
	right := sort(s[mid:])
	sorted := make([]int, 0)
	fmt.Print("Поделили на 2 массива: left", left, " и right", right)

	// сравниваем правый и левый массив
	li, ri := 0, 0 // левый и правый индексы

	// если есть элементы и в левом и в правом слайсе, то поочередно сравниваем их и добавляем в слайс sorted
	for li < len(left) && ri < len(right) {
		if left[li] < right[ri] {
			sorted = append(sorted, left[li])
			li++
		} else {
			sorted = append(sorted, right[ri])
			ri++
		}
	}

	// если элементы в левом слайсе закончились, добавляем в слайс результата элементы с правого слайса
	for li >= len(left) && ri < len(right) {
		sorted = append(sorted, right[ri])
		ri++
	}

	// если элементы в правом слайсе закончились, добавляем в слайс результата элементы с левого слайса
	for ri >= len(right) && li < len(left) {
		sorted = append(sorted, left[li])
		li++
	}
	fmt.Println(" объединили отсортировав: sorted", sorted)
	return sorted
}

func main() {
	orig := []int{9, 3, 6, 1, 0, 10, 2, 5, 4, 8, 7}
	new := sort(orig)
	fmt.Println(new)
}
