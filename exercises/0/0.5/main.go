// Создайте интерфейс Fighter, который содержит методы Attack() и Defend(). Создайте структуру Knight, которая реализует
// интерфейс Fighter и содержит поле силы атаки. Создайте структуру Mage, которая также реализует интерфейс Fighter и содержит
// поле силы магии. Создайте функцию, которая принимает объект типа Fighter и вызывает его методы Attack() и Defend().

package main

import "fmt"

type Fighter interface {
	Attack()
	Defend()
}

type Mage struct {
	power int
}

func (k Mage) Attack() {
	fmt.Println("Маг атаковал! Урон", k.power)
}
func (k Mage) Defend() {
	fmt.Println("Маг отразил удар!")
}

type Knight struct {
	power int
}

func (k Knight) Attack() {
	fmt.Println("Рыцарь атаковал! Урон", k.power)
}
func (k Knight) Defend() {
	fmt.Println("Рыцарь отразил удар!")
}

func main() {
	knight := Knight{13}
	mage := Mage{21}
	fun(knight)
	fun(mage)
}

func fun(i Fighter) {
	i.Defend()
	i.Attack()
}
