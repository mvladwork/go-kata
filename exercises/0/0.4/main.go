// Создайте интерфейс Transporter, который содержит метод Transport() для перемещения объекта в место назначения.
// Создайте структуру Car, которая содержит поле название и метод Transport(), который выводит сообщение о перемещении машины
// в место назначения. Создайте структуру Plane, которая также содержит поле название и метод Transport(), который выводит
// сообщение о перемещении самолета в место назначения. Используйте мапу для хранения списка транспортных средств.
// Напишите функцию для перемещения всех транспортных средств в место назначения.

package main

import "fmt"

type Transporter interface {
	Transport()
}

type Car struct {
	name string
}

func (c Car) Transport() {
	fmt.Println("Машина", c.name, "переместилась!")
}

type Plane struct {
	name string
}

func (c Plane) Transport() {
	fmt.Println("Самолет", c.name, "переместился!")
}

func main() {
	transports := map[int]Transporter{
		1: Car{"Toyota"},
		2: Car{"Mazda"},
		3: Plane{"Airbus"},
	}
	fun(transports)
}

func fun(i map[int]Transporter) {
	for _, v := range i {
		v.Transport()
	}
}
