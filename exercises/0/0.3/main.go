// Создайте структуру Animal, которая содержит поля имя, вид и список характеристик. Каждая характеристика представляется
// в виде структуры Characteristic, которая содержит поле название и значение. Используйте мапу для хранения списка животных.
// Напишите функцию для вывода всех животных и их характеристик.

package main

import "fmt"

type Characteristic struct {
	name  string
	value string
}

type Animal struct {
	Name      string
	Kind      string
	Character []Characteristic
}

func main() {
	animals := map[int]Animal{
		1: {
			"Cat",
			"Кошка",
			[]Characteristic{
				{"Цвет", "Черный"},
				{"Голос", "Мяу"},
			},
		},
		2: {
			"Dog",
			"Собака",
			[]Characteristic{
				{"Цвет", "Белый"},
				{"Голос", "Гав"},
			},
		},
	}
	showAnimals(animals)
}

func showAnimals(a map[int]Animal) {
	for _, v := range a {
		fmt.Println(v)
	}
}
