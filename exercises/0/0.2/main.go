// Создайте структуру Student, которая содержит поля имя, возраст и список курсов, которые студент проходит.
// Каждый курс представляется в виде структуры Course, которая содержит поля название курса, описание и оценку.
// Используйте мапу для хранения списка студентов. Напишите функцию для вывода всех студентов и их курсов с оценками.

package main

import "fmt"

type Course struct {
	name        string
	description string
	grade       int
}

type Student struct {
	name    string
	age     int
	courses []Course
}

func main() {
	var students = map[int]Student{
		1: {
			name: "Игорь",
			age:  19,
			courses: []Course{
				{name: "История", description: "Курс истории", grade: 4},
				{name: "Алгебра", description: "Курс алгебры", grade: 5},
				{name: "Геометрия", description: "Курс геометрии", grade: 5},
			},
		},
		2: {
			name: "Виктор",
			age:  20,
			courses: []Course{
				{name: "История", description: "Курс истории", grade: 3},
				{name: "Алгебра", description: "Курс алгебры", grade: 5},
				{name: "Геометрия", description: "Курс геометрии", grade: 4},
			},
		},
	}

	for _, v := range students {
		fmt.Println(v)
	}
}
