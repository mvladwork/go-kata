// Напишите структуру Student, которая содержит поля name, age и grade. Напишите метод, который печатает информацию о
// студенте в формате "Имя: {name}, Возраст: {age}, Класс: {grade}".

package main

import "fmt"

type Student struct {
	name  string
	age   int
	grade float64
}

func (s Student) showStudent() {
	fmt.Printf("Имя: %v, Возраст: %v, Класс: %v", s.name, s.age, s.grade)
}

func main() {
	s := Student{"Макар", 14, 4.34}
	s.showStudent()
}
