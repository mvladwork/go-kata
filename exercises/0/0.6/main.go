// Создайте структуру User, которая содержит поля имя и возраст. Создайте метод для структуры User, который возвращает true,
// если пользователь совершеннолетний (возраст >= 18). Создайте интерфейс Eligible, который содержит метод IsEligible().
// Реализуйте метод IsEligible() для структуры User. Создайте функцию, которая принимает объект типа Eligible и выводит
// сообщение "User is eligible" или "User is not eligible", в зависимости от результата вызова метода IsEligible().
package main

import "fmt"

type Eligible interface {
	IsEligible() bool
}

type User struct {
	name string
	age  int
}

func (u User) IsEligible() bool {
	return u.age >= 18
}

func main() {
	user := User{"Ваня", 17}
	check(user)
}

func check(i Eligible) {
	if i.IsEligible() {
		fmt.Println("User is eligible")
	} else {
		fmt.Println("User is not eligible")
	}
}
