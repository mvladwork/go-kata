// Напишите функцию, которая проверяет, содержит ли map значение, удовлетворяющее заданному условию.
package main

import "fmt"

func main() {
	m := map[string]int{
		"Андрей":  3,
		"Виталик": 6,
		"Гена":    2,
		"Юра":     5,
		"Петя":    8,
		"Вася":    12,
	}
	fmt.Println(checkMap(m))
}

// проверка, есть ли в мапе значение больше 10
func checkMap(m map[string]int) bool {
	for _, v := range m {
		if v > 10 {
			return true
		}
	}
	return false
}
