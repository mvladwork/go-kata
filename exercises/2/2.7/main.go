// Напишите функцию, которая возвращает сумму всех значений в map.
package main

import "fmt"

func main() {
	m := map[string]int{
		"Андрей":  3,
		"Виталик": 6,
		"Гена":    2,
		"Юра":     5,
		"Петя":    8,
		"Вася":    12,
	}
	fmt.Println(m)
	fmt.Println(sumMap(m))
}

func sumMap(m map[string]int) int {
	sum := 0
	for _, v := range m {
		sum += v
	}
	return sum
}
