// Напишите функцию, которая сортирует map по значениям в порядке возрастания и возвращает новый map.

package main

import (
	"fmt"
	"sort"
)

func main() {

	m := map[string]int{"Андрей": 3, "Виталик": 6, "Гена": 2, "Юра": 5, "Петя": 8, "Вася": 12}

	values := make([]int, 0, len(m))
	for _, k := range m {
		values = append(values, k)
	}
	sort.Ints(values)

	fmt.Println(values)
}
