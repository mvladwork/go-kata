// Напишите функцию, которая удаляет из map все элементы, ключи которых удовлетворяют заданному условию.
package main

import (
	"fmt"
)

func main() {
	m := map[string]int{"Андрей": 3, "Виталик": 6, "Гена": 2, "Юра": 5, "Петя": 8, "Вася": 12}
	fmt.Println(m)
	fmt.Println(newMap(m))
}

func newMap(m map[string]int) map[string]int {
	for i := range m {
		if i == "Андрей" || i == "Вася" {
			delete(m, i)
		}
	}
	return m
}
