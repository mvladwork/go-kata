// Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map переведены в нижний регистр.
package main

import (
	"fmt"
	"strings"
)

func main() {
	m := map[string]string{
		"Андрей":  "Отлично",
		"Виталик": "Удовлетворительно",
		"Гена":    "Хорошо",
		"Юра":     "Отлично",
		"Петя":    "Хорошо",
		"Вася":    "Хорошо",
	}
	fmt.Println(m)
	fmt.Println(newMap(m))
}

func newMap(m map[string]string) map[string]string {
	nm := make(map[string]string)
	for i, v := range m {
		nm[strings.ToLower(i)] = strings.ToLower(v)
	}
	return nm
}
