// Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map поменяны местами.
package main

import "fmt"

func main() {
	m := map[string]int{
		"Андрей":  3,
		"Виталик": 6,
		"Гена":    2,
		"Юра":     5,
		"Петя":    8,
		"Вася":    12,
	}
	fmt.Println(m)
	fmt.Println(newMap(m))
}

func newMap(m map[string]int) map[int]string {
	nm := make(map[int]string)
	for i, v := range m {
		nm[v] = i
	}
	return nm
}
