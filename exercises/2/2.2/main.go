// Напишите функцию, которая объединяет два map[string]int и возвращает новый map, содержащий значения из обоих исходных map,
//если ключи не дублируются. Если ключи дублируются, то значение должно быть суммой значений для каждого ключа.

package main

import "fmt"

func main() {
	m1 := map[string]int{
		"Андрей":  3,
		"Виталик": 6,
		"Гена":    2,
		"Юра":     5,
	}
	m2 := map[string]int{
		"Виталик": 4,
		"Юра":     7,
		"Петя":    8,
	}
	fmt.Println(mergeMaps(m1, m2))
}

func mergeMaps(m1 map[string]int, m2 map[string]int) map[string]int {
	for i, v := range m2 {
		m1[i] += v
	}
	return m1
}
