// Напишите функцию, которая возвращает новый map, содержащий только те элементы, ключи которых удовлетворяют заданному условию.
package main

import "fmt"

func main() {
	m := map[string]int{
		"Андрей":  3,
		"Виталик": 6,
		"Гена":    2,
		"Юра":     5,
		"Петя":    8,
		"Вася":    12,
	}
	fmt.Println(newMap(m))
}

// условия: оставить только те элементы, ключи которые не равны строке "Андрей"
func newMap(m map[string]int) map[string]int {
	nm := make(map[string]int)
	for i, v := range m {
		if i != "Андрей" {
			nm[i] = v
		}
	}
	return nm
}
