// Напишите функцию, которая возвращает новый map, в котором значения исходного map умножены на заданный множитель.

package main

import "fmt"

func main() {

	m := map[string]int{"rub": 15000, "usd": 1200, "eur": 900, "btc": 1}
	nm := make(map[string]int)
	k := 7
	for i, v := range m {
		nm[i] = v * k
	}
	fmt.Println(nm)

}
