// Напишите функцию, которая считает количество вхождений каждого элемента в слайс и возвращает результат в виде map[string]int,
// где ключ - это элемент, а значение - количество его вхождений.
package main

import "fmt"

func main() {
	list := []string{"Андрей", "Виталик", "Гена", "Юра", "Виталик", "Юра", "Петя", "Игорь", "Юра", "Игорь"}
	fmt.Println(countEntriesInMap(list))
}

func countEntriesInMap(list []string) map[string]int {
	newList := make(map[string]int)
	for _, v := range list {
		newList[v] += 1
	}
	return newList
}
