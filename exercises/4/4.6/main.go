// Напишите функцию, которая принимает на вход два значения любого типа и возвращает true, если они равны, и false в противном

package main

import "fmt"

func main() {
	i := 4
	i2 := 4
	fmt.Println(check(i, i2))
}

func check(i interface{}, i2 interface{}) bool {
	return i == i2
}
