// Напишите интерфейс Sortable, который определяет метод Len() для получения длины списка, метод Swap() для обмена элементами
//и метод Less() для сравнения двух элементов списка.

package main

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

func main() {

}
