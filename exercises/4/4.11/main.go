// Напишите интерфейс Sortable, который определяет метод Len() для получения длины списка, метод Swap() для обмена элементами
// и метод Less() для сравнения двух элементов списка.

package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Employee struct {
	Name       string
	Age        int
	Salary     float64
	Department string
}

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type ByAge []Employee

func (a ByAge) Len() int {
	return len(a)
}
func (a ByAge) Less(i, j int) bool {
	return a[i].Age < a[j].Age
}
func (a ByAge) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type BySalary []Employee

func (a BySalary) Len() int {
	return len(a)
}
func (a BySalary) Less(i, j int) bool {
	return a[i].Salary < a[j].Salary
}
func (a BySalary) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type ByDepartment []Employee

func (a ByDepartment) Len() int {
	return len(a)
}
func (a ByDepartment) Less(i, j int) bool {
	return a[i].Department < a[j].Department
}
func (a ByDepartment) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func main() {
	employees := generateRandomEmployees(10)
	for {
		printMenu()
		var choice int
		fmt.Scan(&choice)

		switch choice {
		case 1:
			printEmployees(employees)
		case 2:
			sort.Sort(ByAge(employees))
			printEmployees(employees)
		case 3:
			sort.Sort(BySalary(employees))
			printEmployees(employees)
		case 4:
			sort.Sort(ByDepartment(employees))
			printEmployees(employees)
		case 5:
			fmt.Println("Goodbye!")
			return
		default:
			fmt.Println("Invalid choice!")
		}
	}
}

func generateRandomEmployees(n int) []Employee {
	names := map[int]string{
		0:  "Андрей",
		1:  "Алексей",
		2:  "Виктор",
		3:  "Виталий",
		4:  "Владимир",
		5:  "Геннадий",
		6:  "Денис",
		7:  "Игорь",
		8:  "Константин",
		9:  "Михаил",
		10: "Олег",
		11: "Петр",
		12: "Руслан",
		13: "Станислав",
		14: "Сергей",
		15: "Эдуард",
		16: "Юрий",
		17: "Ян",
		18: "Леонид",
		19: "Николай",
		20: "Кирилл",
	}
	departments := map[int]string{
		0: "Отдел продаж",
		1: "Бухгалтерия",
		2: "IT отдел",
		3: "Юридический отдел",
		4: "Отдел технической поддержки",
		5: "Администрация",
		6: "Отдел маркетинга",
		7: "Отдел кадров",
	}

	employees := make([]Employee, 0, 10)
	for i := 0; i < n; i++ {
		rand.Seed(time.Now().UnixNano())
		employee := Employee{
			Name:       names[rand.Intn(21)],
			Age:        rand.Intn(50-25+1) + 25,
			Salary:     float64(rand.Intn(100-30+1)+30) * 1000,
			Department: departments[rand.Intn(8)],
		}
		employees = append(employees, employee)
	}

	return employees
}

func printMenu() {
	fmt.Println("Please choose an option:")
	fmt.Println("1.1. Print all employees")
	fmt.Println("1.2. Sort employees by age")
	fmt.Println("3. Sort employees by salary")
	fmt.Println("4. Sort employees by department")
	fmt.Println("5. Exit")
}

func printEmployees(employees []Employee) {
	fmt.Println("СПИСОК РАБОТНИКОВ:")
	for _, v := range employees {
		fmt.Println("Имя:", v.Name, "Возраст:", v.Age, "Зарплата:", v.Salary, "Отдел:", v.Department)
	}
}
