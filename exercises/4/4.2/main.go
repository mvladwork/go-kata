// Напишите функцию, которая принимает на вход любой тип данных и возвращает true, если это строка, и false в противном случае

package main

import "fmt"

func main() {
	v := "qwe"
	fmt.Println(check(v))
}

func check(i interface{}) bool {
	switch i.(type) {
	case string:
		return true
	}
	return false
}
