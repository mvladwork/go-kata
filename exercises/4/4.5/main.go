// Напишите функцию, которая принимает на вход интерфейс Shape и выводит его площадь и периметр.

package main

import "fmt"

type square struct{}

func (square) area(x int, y int) int {
	return x * y
}
func (square) perimeter(x int, y int) int {
	return (x + y) * 2
}

type Shape interface {
	area(x int, y int) int
	perimeter(x int, y int) int
}

func main() {
	var x square
	show(x)
}

func show(f Shape) {
	fmt.Println(f.area(3, 5), f.perimeter(3, 5))
}
