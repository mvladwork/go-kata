// Напишите функцию, которая сортирует список элементов любого типа данных, реализующих интерфейс Sortable.

package main

import (
	"fmt"
	"sort"
)

type Any []string

func (a Any) Len() int {
	return len(a)
}
func (a Any) Less(i, j int) bool {
	return a[i] > a[j]
}
func (a Any) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func main() {
	var s Any = []string{"анна", "яна", "фаина", "боня", "луиза"}
	fmt.Println(s)
	sort.Sort(s)
	fmt.Println(s)

}
