// Напишите функцию, которая принимает на вход любой тип данных и выводит его тип.
package main

import "fmt"

func main() {
	v := "qwe"
	iPrint(v)
}

func iPrint(i interface{}) {
	fmt.Printf("%T", i)
}
