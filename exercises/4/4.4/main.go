// Напишите функцию, которая принимает на вход любой тип данных и выводит его значения в строковом формате.

package main

import "fmt"

func main() {
	v := "qwe"
	check(v)
}

func check(i interface{}) {
	fmt.Printf("%v", i)
}
