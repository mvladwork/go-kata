// Напишите функцию, которая возвращает новый слайс, состоящий из первых N элементов исходного слайса.
package main

import "fmt"

func main() {
	list := []int{16, 23, 83, 49, 12, 66, 74, 16, 12, 190}
	num := 3
	fmt.Println(modifySlice(list, num))

}

func modifySlice(list []int, num int) []int {
	newList := make([]int, 0, num)
	for i := 0; i < num; i++ {
		newList = append(newList, list[i])
	}
	return newList
}
