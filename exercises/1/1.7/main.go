// Напишите функцию, которая удаляет из слайса все элементы, не удовлетворяющие заданному условию.
package main

import (
	"fmt"
)

func main() {
	list := []int{16, 23, 83, 49, 12, 66, 74, 16, 12, 190}
	fmt.Println(modifySlice(list))
}

// Условия, оставляем только четные числа, которые меньше 100
func modifySlice(list []int) []int {
	i := 0
	for _, v := range list {
		if v%2 == 0 && v < 100 {
			list[i] = v
			i++
		}
	}
	return list[:i]
}
