// Напишите функцию, которая объединяет два слайса целых чисел и удаляет дубликаты.
package main

import (
	"fmt"
	"sort"
)

func main() {
	list := []int{16, 29, 97, 49, 29, 66, 74, 29, 54, 19}
	list2 := []int{29, 45, 19, 54, 44, 79, 80}
	fmt.Println(mergeAndDelDuplicates(list, list2))

}

func mergeAndDelDuplicates(list []int, list2 []int) []int {
	newList := append(list, list2...)
	sort.Ints(newList)

	i := 0
	for j := 1; j < len(newList); j++ {
		if newList[i] != newList[j] {
			i++
			newList[i] = newList[j]
		}
	}

	return newList[:i+1]
}
