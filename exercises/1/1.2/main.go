// Напишите функцию, которая удаляет дубликаты из слайса строк

package main

import (
	"fmt"
	"sort"
)

func main() {
	list := []string{"Андрей", "Виталик", "Гена", "Юра", "Виталик", "Юра", "Петя", "Игорь", "Юра", "Игорь"}
	fmt.Println(list)
	list = delDuplicates(list)
	fmt.Println(list)
}

func delDuplicates(list []string) []string {
	sort.Strings(list)
	i := 0
	for j := 1; j < len(list); j++ {
		if list[j] != list[i] {
			i++
			list[i] = list[j]
		}
	}
	return list[:i+1]
}

//func delDuplicates(list []string) []string {
//	newList := make([]string, 0, 10) // слайс с уникальными строками
//	// проходим по всем именам
//	for i, v := range list {
//		m := false
//		// проходим по всем уникальным именам
//		// если есть совпадение, то метку ставим в true и заканчиваем цикл, потому что это дубль
//		for _, v2 := range newList {
//			if v == v2 {
//				m = true
//				break
//			}
//		}
//		// если не было дублей (m == false), то добавляем строку в слайс с уникальными строками
//		if !m {
//			newList = append(newList, list[i])
//		}
//	}
//	return newList
//}

/*
// Реализация через map
func delDuplicatesMap(list []string) []string {
	newList := map[string]bool{}
	for i := range list {
		if _, ok := newList[list[i]]; ok {
			continue
		} else {
			newList[list[i]] = true
		}
	}

	list = make([]string, 0, 10)
	for i := range newList {
		list = append(list, i)
	}
	return list
}
*/
