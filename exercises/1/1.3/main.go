// Напишите функцию, которая сортирует слайс строк в порядке возрастания.
package main

import (
	"fmt"
	"sort"
)

func main() {
	list := []string{"Игорь", "Виталик", "Андрей", "Гена", "Юра", "Петя"}
	fmt.Println(list)
	sortAZ(list)
	fmt.Println(list)
}

func sortAZ(list []string) []string {
	sort.Strings(list)
	return list
}
