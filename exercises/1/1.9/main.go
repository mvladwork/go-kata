// Напишите функцию, которая возвращает новый слайс, состоящий из элементов исходного слайса в обратном порядке.
package main

import "fmt"

func main() {
	a := []int{16, 23, 83, 49, 12, 66, 74, 16, 12, 190}
	fmt.Println(a)
	revSlice(a)
	fmt.Println(a)
}

func revSlice(a []int) {

	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}

}
