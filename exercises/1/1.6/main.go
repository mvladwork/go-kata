// Напишите функцию, которая проверяет, содержит ли слайс хотя бы один элемент, удовлетворяющий заданному условию.

package main

import "fmt"

func main() {
	list := []int{16, 23, 83, 49, 12, 66, 74, 29, 54, 190}
	fmt.Println(checkSlice(list))

}

// условие: хотя бы один элемент должен быть больше 100
func checkSlice(list []int) bool {
	for _, v := range list {
		if v > 100 {
			return true
		}
	}
	return false
}
