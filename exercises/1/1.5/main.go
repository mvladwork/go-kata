// Напишите функцию, которая возвращает новый слайс, состоящий только из четных чисел из исходного слайса.
package main

import "fmt"

func main() {
	list := []int{16, 23, 83, 49, 12, 66, 74, 29, 54, 19}
	fmt.Println(newSlice(list))
}

func newSlice(list []int) []int {
	newList := make([]int, 0, len(list))
	for _, v := range list {
		if v%2 == 0 {
			newList = append(newList, v)
		}
	}
	return newList
}
