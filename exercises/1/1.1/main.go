// Напишите функцию, которая находит максимальный элемент в слайсе целых чисел.

package main

import (
	"fmt"
	"sort"
)

func main() {
	list := []int{16, 23, 83, 49, 12, 66, 74, 29, 54, 19}
	max := maxElement(list)
	fmt.Println(max)
}

func maxElement(list []int) int {
	sort.Ints(list)
	return list[len(list)-1]
}

/*
func maxElement(list []int) int {
	max := list[0]
	for _, v := range list {
		if v > max {
			max = v
		}
	}
	return max
}
*/
