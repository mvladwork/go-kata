// Напишите функцию, которая удаляет из слайса все элементы, равные нулю.
package main

import "fmt"

func main() {
	list := []int{16, 0, 83, 49, 0, 66, 74, 16, 0, 190}
	fmt.Println(modifySlice(list))
}

// Условия, удаляем все 0 в слайсе
func modifySlice(list []int) []int {
	i := 0
	for _, v := range list {
		if v != 0 {
			list[i] = v
			i++
		}
	}
	return list[:i]
}
