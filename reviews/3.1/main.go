package main

import (
	"fmt"
	"math/rand"
	"reflect"
	"testing"
	"time"
)

func main() {
	//randomData: count - количество генерируемых элементов, max - генерация от 0 до max
	s1 := randomData(99, 10)
	fmt.Println("first slice: ", s1)
	s2 := randomData(49, 10)
	fmt.Println("second slice: ", s2)
	res := merge(s1, s2)
	fmt.Println("result: ", res)
	fmt.Println(check(res))
}

func check(s []int) bool {
	for i := 1; i < len(s); i++ {
		if s[i] < s[i-1] {
			return false
		}
	}
	return true
}

func merge(left []int, right []int) []int {
	left = bubbleSort(left)
	//fmt.Println(left)
	right = bubbleSort(right)
	//fmt.Println(right)

	var res []int
	var i, j int
	// сравниваем элементы до тех пор, пока не будет проверен последний элемент левого или правого слайса
	for i < len(left) && j < len(right) {
		if left[i] < right[j] {
			res = append(res, left[i])
			i++
		} else {
			res = append(res, right[j])
			j++
		}
	}
	// если остались элементы в левом слайсе, добавляем их в результат
	for ; i < len(left); i++ {
		res = append(res, left[i])
	}
	// если остались элементы в правом слайсе, добавляем их в результат
	for ; j < len(right); j++ {
		res = append(res, right[j])
	}

	return res
}

func randomData(max int, count int) []int {
	rand.Seed(time.Now().UnixNano())
	var res []int
	for i := 0; i < count; i++ {
		res = append(res, rand.Intn(max))
	}
	return res
}

func bubbleSort(data []int) []int {
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(data)-i; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true
			}
		}
		i++
	}
	return data
}

func TestReview(t *testing.T) {
	type args struct {
		data1 []int
		data2 []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort check",
			args: args{
				data1: []int{55, 21, 3, 66},
				data2: []int{45, 4, 44, 99},
			},
			want: []int{3, 4, 21, 44, 45, 55, 66, 99},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := merge(tt.args.data1, tt.args.data2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("merge() = %v, want %v", got, tt.want)
			}
		})
	}
}
