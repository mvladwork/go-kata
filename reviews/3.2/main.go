package main

import (
	"errors"
	"fmt"
)

// TreeNode структура бинарного дерева
type TreeNode struct {
	val   int
	left  *TreeNode
	right *TreeNode
}

func main() {
	t := &TreeNode{val: 15}
	_ = t.Insert(12)
	_ = t.Insert(20)
	_ = t.Insert(8)
	_ = t.Insert(13)
	_ = t.Insert(14)
	fmt.Println(t.Find(15))
	t.Delete(12)
	t.PrintInOrder()
	fmt.Printf("min is %d\n", t.FindMin())
	fmt.Printf("max is %d\n", t.FindMax())
}

// PrintInOrder вывести все элементы
func (t *TreeNode) PrintInOrder() {
	if t == nil {
		return
	}
	t.left.PrintInOrder()
	fmt.Println(t.val)
	t.right.PrintInOrder()
}

// Insert вставка нового элемента в дерево
func (t *TreeNode) Insert(value int) error {
	if t == nil {
		return errors.New("дерево имеет значение nil")
	}
	if t.val == value {
		return errors.New("такое значение в дереве уже существует")
	}
	if t.val > value {
		if t.left == nil {
			t.left = &TreeNode{val: value}
			return nil
		}
		return t.left.Insert(value)
	}
	if t.val < value {
		if t.right == nil {
			t.right = &TreeNode{val: value}
			return nil
		}
		return t.right.Insert(value)
	}
	return nil
}

// Find поиск элемента со значением value в дереве
func (t *TreeNode) Find(value int) (TreeNode, bool) {
	if t == nil {
		return TreeNode{}, false
	}
	if value == t.val {
		return *t, true
	} else if value < t.val {
		return t.left.Find(value)
	} else {
		return t.right.Find(value)
	}
}

// Delete удалить элемент из дерева
func (t *TreeNode) Delete(value int) {
	t.remove(value)
}

func (t *TreeNode) remove(value int) *TreeNode {
	if t == nil {
		return nil
	}
	// ищем ноду со значением value
	if value < t.val {
		t.left = t.left.remove(value)
		return t
	}
	if value > t.val {
		t.right = t.right.remove(value)
		return t
	}
	// нода для удаления нашлась, теперь удаляем ее
	// если нода является листом, то удаляем саму ноду, у родителя ветка поменяется
	// на nil (t.left = t.left.remove(value))
	if t.left == nil && t.right == nil {
		//goland:noinspection ALL
		t = nil
		return nil
	}
	// если у ноды есть только правая ветка, то перезаписываем текущую ноду на правую
	if t.left == nil {
		//goland:noinspection ALL
		t = t.right
		return t
	}
	// если у ноды есть только левая ветка, то перезаписываем текущую ноду на левую
	if t.right == nil {
		//goland:noinspection ALL
		t = t.left
		return t
	}
	// если ничего вышеперечисленное не сработало, значит у удаляемой ноды две ветки
	// в таком случае находим наименьшее значение в правой ветке и перезаписываем
	// текущую ноду на нее
	min := t.right
	for {
		if min != nil && min.left != nil {
			min = min.left
		} else {
			break
		}
	}
	// записываем значение min как текущее значение и удаляем в правой ветке ноду
	// с min значением
	t.val = min.val
	t.right = t.right.remove(t.val)
	return t
}

// FindMax поиск максимального элемента
func (t *TreeNode) FindMax() int {
	if t.right == nil {
		return t.val
	}
	return t.right.FindMax()
}

// FindMin поиск минимального элемента
func (t *TreeNode) FindMin() int {
	if t.left == nil {
		return t.val
	}
	return t.left.FindMin()
}
