package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//

func RandNumbers(length, max int) []int {
	var s []int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < length; i++ {
		s = append(s, rand.Intn(max))
	}

	return s
}

func writeToChan(ch chan<- int) {
	defer close(ch)
	for _, v := range RandNumbers(100, 100) {
		ch <- v
	}
}

func mergeChan(chs ...chan int) chan int {
	res := make(chan int)

	go func() {

		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int) {
				defer wg.Done()
				for id := range ch {
					res <- id
				}
			}(ch)
		}

		wg.Wait()
		close(res)
	}()

	return res
}

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	ch4 := make(chan int)

	mergedChan := mergeChan(ch1, ch2, ch3, ch4)
	go writeToChan(ch1)
	go writeToChan(ch2)
	go writeToChan(ch3)
	go writeToChan(ch4)

	for v := range mergedChan {
		fmt.Println(v)
	}

}
