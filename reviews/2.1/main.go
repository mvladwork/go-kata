package main

import (
	"fmt"
	"sync"
)

type Cash struct {
	d map[int]string
	//mu *sync.Mutex
}

func (c *Cash) set(i int, s string) {
	//c.mu.Lock()
	c.d[i] = s
	//c.mu.Unlock()
}

func (c *Cash) get(i int) string {
	//c.mu.Lock()
	//defer c.mu.Unlock()
	return c.d[i]
}

func main() {

	c := Cash{
		d: make(map[int]string),
		//mu: &sync.Mutex{},
	}
	wg := sync.WaitGroup{}
	for i := 1; i <= 100; i++ {
		wg.Add(1)
		go func(i int) {
			c.set(i, fmt.Sprint("element ", i))
			fmt.Println(c.get(i))
			wg.Done()
		}(i)

	}
	wg.Wait()

}
