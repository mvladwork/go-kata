package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.
type User struct {
	Age      int
	Income   int
	FullName string
}

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	// используй rand.Intn
	return rand.Intn(70-18+1) + 18
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	// используй rand.Intn()
	return rand.Intn(500001)
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	names := []string{
		"Андрей",
		"Алексей",
		"Виктор",
		"Виталий",
		"Владимир",
		"Геннадий",
		"Денис",
		"Игорь",
		"Константин",
		"Михаил",
		"Олег",
		"Петр",
		"Руслан",
		"Станислав",
		"Сергей",
		"Эдуард",
		"Юрий",
		"Ян",
		"Леонид",
		"Николай",
		"Кирилл",
	}
	surnames := []string{
		"Андреев",
		"Алексеев",
		"Попов",
		"Сидоров",
	}

	// используй rand.Intn() для выбора случайного имени и фамилии.
	// создай слайс с именами и слайс с фамилиями.
	return names[rand.Intn(len(names))] + " " + surnames[rand.Intn(len(surnames))]

}

func main() {

	// Сгенерируй 1000 пользователей и заполни ими слайс users.
	users := make([]User, 0, 1000)
	sumAge := 0
	sumIncome := 0
	for i := 0; i < 1000; i++ {
		user := User{
			FullName: generateFullName(),
			Age:      generateAge(),
			Income:   generateIncome(),
		}
		users = append(users, user)
		sumAge += user.Age
		sumIncome += user.Income
	}
	//fmt.Println(users)

	avrAge := sumAge / 1000
	avrIncome := sumIncome / 1000

	num := 0
	for _, v := range users {
		if v.Income > avrIncome {
			num++
		}
	}

	sort.Slice(users, func(i, j int) bool {
		return users[i].Age < users[j].Age
	})
	fmt.Println(users)

	// Выведи средний возраст пользователей.
	// Выведи средний доход пользователей.
	// Выведи количество пользователей, чей доход превышает средний доход.
	fmt.Println("Средний возраст пользователей:", avrAge)
	fmt.Println("Выведи средний доход пользователей:", avrIncome)
	fmt.Println("Количество пользователей, чей доход превышает средний доход:", num)

}
