module gitlab.com/mvladwork/go-kata

go 1.19

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	gitlab.com/mvladwork/greet v0.0.0-20230411064409-9b0cdbdb53af
)

require (
	fyne.io/fyne/v2 v2.3.5 // indirect
	github.com/essentialkaos/translit/v2 v2.0.5 // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
