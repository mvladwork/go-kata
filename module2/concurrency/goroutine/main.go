package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	for i := 0; i < 5; i++ {
		go func() {
			fmt.Printf("int {i}\n")
		}()
		runtime.Gosched()
	}
	time.Sleep(time.Second * 1)
}

// WAIT GROUP
//func main() {
//	wg := new(sync.WaitGroup)
//	//wg.Add(5) // вариант 1
//	for i := 1; i <= 5; i++ {
//		wg.Add(1)
//		go longOperation(i, wg)
//	}
//	wg.Wait()
//}
//func longOperation(i int, wg *sync.WaitGroup) {
//	fmt.Println(i)
//	wg.Done()
//}

//func workAndPrint(num int) {
//	fmt.Printf("start job #%v\n", num)
//	var calc int
//	for i := 0; i < 1000; i++ {
//		calc = i * num
//	}
//	fmt.Printf("end job #%v: calc = %v\n", num, calc)
//}
//
//func main() {
//	for i := 1; i <= 5; i++ {
//		go workAndPrint(i)
//	}
//	time.Sleep(time.Millisecond)
//
//}

//func main() {
// количество горутин, которые могут выполняться одновременно
//runtime.GOMAXPROCS(1)
// сколько горутин могут выполняться одновременно
//fmt.Println(runtime.NumCPU())

//go showNumbers(100)
// вручную переключиться на другую горутину
//runtime.Gosched()

//time.Sleep(time.Second)
//fmt.Println("exit")
//withWait()
//writeWithoutConcurrent()
//}

//func writeWithoutConcurrent() {
//	start := time.Now()
//	var counter int
//
//	for i := 0; i < 10000; i++ {
//		time.Sleep(time.Nanosecond)
//		counter++
//	}
//	fmt.Println(counter)
//	fmt.Println(time.Since(start).Seconds())
//}

//func showNumbers(num int) {
//	for i := 0; i < num; i++ {
//		fmt.Println(i)
//	}
//}

//func withWait() {
//	var wg sync.WaitGroup
//
//	for i := 0; i < 10; i++ {
//		wg.Add(1)
//
//		go func(i int) {
//			fmt.Println(i + 1)
//			wg.Done()
//		}(i)
//	}
//
//	wg.Wait()
//	fmt.Println("exit")
//}

//func withoutWait() {
//	for i := 0; i < 10; i++ {
//		go fmt.Println(i + 1)
//	}
//}
