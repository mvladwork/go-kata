package main

import (
	"fmt"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

type Cache struct {
	data map[string]interface{}
	init bool
	mu   *sync.Mutex
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
		mu:   &sync.Mutex{},
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	c.mu.Lock()
	c.data[key] = v
	c.mu.Unlock()
	return nil
}

func (c *Cache) Get(key string) interface{} {
	if !c.init {
		return nil
	}
	return c.data[key]
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	var eg errgroup.Group
	for i := range keys {
		idx := i
		eg.Go(func() error {
			err := cache.Set(keys[idx], idx)
			return err
		})
	}

	time.Sleep(100 * time.Millisecond)
	for k, v := range cache.data {
		fmt.Println(k, v)
	}

	err := eg.Wait()
	if err != nil {
		panic(err)
	}

}
