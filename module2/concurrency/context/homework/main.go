package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
	go func(ctx context.Context) {
		defer func() {
			close(a)
			close(c)
			close(b)
		}()
		for num := range out {
			select {
			case <-ctx.Done():
				cancel()
				return
			case a <- num:
			case b <- num:
			case c <- num:
			}
		}
	}(ctx)

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

	if _, ok := <-a; !ok {
		println("Канал a закрыт")
	} else {
		println("Канал a открыт")
	}

	if _, ok := <-b; !ok {
		println("Канал b закрыт")
	} else {
		println("Канал b открыт")
	}

	if _, ok := <-c; !ok {
		println("Канал c закрыт")
	} else {
		println("Канал c открыт")
	}

	if _, ok := <-mainChan; !ok {
		println("Канал mainChan закрыт")
	} else {
		println("Канал mainChan открыт")
	}

	time.Sleep(3 * time.Second)

}
