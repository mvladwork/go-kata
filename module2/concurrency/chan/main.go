package main

import (
	"fmt"
	"math/rand"
	"time"
)

func longTimeRequest() <-chan int {
	r := make(chan int)
	go func() {
		time.Sleep(3 * time.Second)
		r <- rand.Intn(100)
	}()
	return r
}

func sumSquares(a, b int) int {
	return a*a + b*b
}

func main() {
	rand.Seed(time.Now().UnixNano())

	a, b := longTimeRequest(), longTimeRequest()
	fmt.Println(sumSquares(<-a, <-b))
}

/*
func sendData(ctx context.Context, num int) {
	timer := time.NewTimer(time.Duration(num) * time.Second)

	select {
	case <-ctx.Done():
		fmt.Printf("Процесс #%v отменен\n", num)
		return
	case <-timer.C:
		fmt.Printf("Данные процесса #%v успешно отправлены\n", num)
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	for i := 1; i <= 10; i++ {
		go sendData(ctx, i)
	}
	time.Sleep(3 * time.Second)
	cancel()
	time.Sleep(500 * time.Millisecond)
}
*/
//
//func jobWithTimeout(t *time.Timer, q chan int) {
//	time.Sleep(1 * time.Second)
//	select {
//	case <-t.C:
//		fmt.Println("Время вышло")
//	case <-q:
//		if !t.Stop() {
//			<-t.C
//		}
//		fmt.Println("Таймер остановлен")
//	default:
//		fmt.Println("Завершение функции")
//	}
//
//}
//
//func main() {
//	timer := time.NewTimer(3 * time.Second)
//	//fmt.Println(timer)
//	quit := make(chan int)
//	go jobWithTimeout(timer, quit)
//	quit <- 1
//
//	time.Sleep(2 * time.Second)
//}

//func sleep(t time.Duration, wg *sync.WaitGroup) {
//	time.Sleep(t)
//	fmt.Println("sleep", t)
//	wg.Done()
//}
//
//func main() {
//	wg := &sync.WaitGroup{}
//
//	wg.Add(1)
//	go sleep(1*time.Second, wg)
//	wg.Add(1)
//	go sleep(2*time.Second, wg)
//	wg.Add(1)
//	wg.Add(1)
//	go sleep(3*time.Second, wg)
//
//	wg.Wait()
//}

//type Avatar struct {
//	URL  string
//	Size int
//}
//
//type Client struct {
//	ID   int
//	Img  Avatar
//	Name string
//	Age  int64
//}
//
//func addAge(cl *Client, add int, mu *sync.Mutex) {
//	mu.Lock()
//	cl.Age = cl.Age + int64(add)
//	mu.Unlock()
//}
//func main() {
//
//	cl := &Client{}
//
//	mu := &sync.Mutex{}
//	for i := 1; i <= 1000; i++ {
//		go addAge(cl, 1, mu)
//	}
//
//	//time.Sleep(1 * time.Second)
//	fmt.Printf("%#v\n", cl)
//}

//func write(ch chan<- int) {
//	ch <- 100
//}
//
//func read(ch, quit <-chan int) {
//	for {
//		select {
//		case x := <-ch:
//			fmt.Println("ch =", x)
//		case <-quit:
//			fmt.Println("quit")
//			return
//		default:
//			fmt.Println("default")
//		}
//	}
//}
//
//func main() {
//	ch := make(chan int)
//	quit := make(chan int)
//
//	go read(ch, quit)
//
//	go write(ch)
//	//runtime.Gosched()
//
//	go write(quit)
//	time.Sleep(1 * time.Second)
//}

//func writeChan(ch chan<- int) {
//	for i := 1; i <= 5; i++ {
//		ch <- i
//	}
//	close(ch)
//}
//
//func main() {
//	fmt.Println("start main")
//
//	ch := make(chan int)
//
//	go writeChan(ch)
//
//	for i := range ch {
//		fmt.Println("chan i =", i)
//	}
//	//time.Sleep(1 * time.Second)
//
//	fmt.Println("end main")
//}
