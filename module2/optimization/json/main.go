package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

type Welcome []WelcomeElement

func UnmarshalWelcome(data []byte) (Welcome, error) {
	var w Welcome
	err := json.Unmarshal(data, &w)
	return w, err
}

func (w *Welcome) Marshal() ([]byte, error) {
	return json.Marshal(w)
}

func UnmarshalWelcome2(data []byte) (Welcome, error) {
	var w Welcome
	err := jsoniter.Unmarshal(data, &w)
	return w, err
}

func (w *Welcome) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(w)
}

type WelcomeElement struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
