package main

import "testing"

var jData = []byte(`[
        {
            "id": 1,
            "name": "Rex",
			"category": {
				"id": 1,
				"name": "siamese"
			},
            "photoUrls": ["link1", "link2"],
            "tags": [
                {
                    "id": 2,
                    "name": "white"
                }
            ],
            "status": "for sale"
        }
    ]`)

func BenchmarkJsonUnmarshall(b *testing.B) {
	var (
		element Welcome
		err     error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = UnmarshalWelcome(jData)
		if err != nil {
			panic(err)
		}

	}
	_ = element
}

func BenchmarkJsonMarshall(b *testing.B) {
	var (
		element Welcome
		err     error
		data    []byte
	)
	b.ResetTimer()
	data, err = element.Marshal()
	if err != nil {
		panic(err)
	}
	_ = data
}

func BenchmarkJsoniterUnmarshall(b *testing.B) {
	var (
		element Welcome
		err     error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		element, err = UnmarshalWelcome2(jData)
		if err != nil {
			panic(err)
		}

	}
	_ = element
}

func BenchmarkJsoniterMarshall(b *testing.B) {
	var (
		element Welcome
		err     error
		data    []byte
	)
	b.ResetTimer()
	data, err = element.Marshal2()
	if err != nil {
		panic(err)
	}
	_ = data
}
