package main

import (
	"fmt"
	"reflect"
)

type User struct {
	FirstName  string
	SecondName string
	Age        int
}

func allFields(val interface{}) {
	elem := reflect.ValueOf(val)
	for i := 0; i < elem.NumField(); i++ {
		field := elem.Field(i)
		typeName := field.Type().Name()
		fmt.Println(field, typeName)
	}
}

func isEmpty(val interface{}) bool {
	return reflect.ValueOf(val).IsZero()
}
func main() {

	user := User{
		FirstName:  "Ivan",
		SecondName: "Ivanov",
		Age:        10,
	}
	allFields(user)

	a := isEmpty("asdfsd")
	b := isEmpty("")
	c := isEmpty(456)
	d := isEmpty(0)
	println(a, b, c, d)
}
