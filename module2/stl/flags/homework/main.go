package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	path := flag.String("conf", "", "path")
	flag.Parse()

	f, err := os.ReadFile(*path)
	if err != nil {
		fmt.Println("read error:", err)
		os.Exit(1)
	}

	if !json.Valid(f) {
		fmt.Println("invalid json:", err)
		os.Exit(1)
	}

	conf := &Config{}
	if err := json.Unmarshal(f, conf); err != nil {
		fmt.Println("unmarshal operation error:", err)
	}
	fmt.Println("Production:", conf.Production)
	fmt.Println("AppName:", conf.AppName)

}
