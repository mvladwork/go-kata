package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func main() {
	p := Person{Name: "Andy", Age: 18}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	generateSelfStory(p.Name, p.Age, 10.00000025)

}

func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Hello, my name is %v. I'm %v y.o. And I also have $%.2f in my wallet right now.", name, age, money)
}
