package main

import (
	"fmt"
	"runtime"
	"strings"
)

func main() {

	s := "Kata Academy"

	fmt.Println(runtime.NumCPU())
	//s := "Ката академи"

	//Count подсчитывает количество непересекающихся экземпляров substr в s. Если substr является пустой строкой, Count возвращает 1 + количество кодовых точек Unicode в s.
	// Cut вырезает слайсы s вокруг первого экземпляра sep, возвращая текст до и после sep. Найденный результат сообщает появляется ли sep в s. Если sep не появляется в s, cut возвращает s, "", false.

	//Trim возвращает фрагмент строки s со всеми удаленными начальными и конечными кодовыми точками Unicode, содержащимися в cutset.
	//TrimFunc возвращает фрагмент строки s со всеми удаленными начальными и конечными кодовыми точками Unicode c,
	//удовлетворяющими f(c).
	// TrimSpace возвращает фрагмент строки s с удаленными начальными и конечными пробелами, как определено в Unicode.

	//Replace возвращает копию строки s с заменой первых n непересекающихся экземпляров старого на новый.
	//Если old пусто, оно совпадает в начале строки и после каждой последовательности UTF-8,
	//что дает до k+1 замен для строки из k-рун. Если n < 0, количество замен не ограничено.
	fmt.Println(strings.Repeat(s, 2)) // возвращает новую строку, состоящую из количества копий строки s

	//strings.Join, strings.Split и strings.ReplaceAll
	// https://platform.kata.academy/user/courses/3/7/1/3

	fmt.Println(len(s)) // Возвращает количество байт в строке

	fmt.Println(strings.HasPrefix(s, "Kata"))    //Поиск строки с начала
	fmt.Println(strings.HasSuffix(s, "Academy")) //Поиск строки с конца
	fmt.Println(strings.Contains(s, "Aca"))      //Поиск в любом месте строки
	fmt.Println(strings.Count(s, "a"))           //Счётчик количества появлений строки в тексте

	fmt.Println(strings.ToUpper(s)) // верхний регистр
	fmt.Println(strings.ToLower(s)) // нижний регистр
}
