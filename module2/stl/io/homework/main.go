package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"test test",
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	w := new(bytes.Buffer)
	for _, v := range data {
		w.WriteString(v + "\n")
	}

	f, err := os.Create("/tmp/example.txt")
	check(err)
	_, err = io.Copy(f, w)
	check(err)
	f.Close()

	f, err = os.Open("/tmp/example.txt")
	check(err)
	defer f.Close()

	w2 := new(bytes.Buffer)
	_, err = io.Copy(w2, f)
	check(err)

	fmt.Println(w2.String())
}
