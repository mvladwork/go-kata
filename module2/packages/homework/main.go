package main

import (
	"fmt"

	"gitlab.com/mvladwork/greet"
)

func main() {
	greet.Hello()
	fmt.Println(greet.World)

	var oct = greet.Octopus{
		Name:  "Jesse",
		Color: "orange",
	}
	fmt.Println(oct.String())
	oct.Reset()
	fmt.Println(oct.String())
}
