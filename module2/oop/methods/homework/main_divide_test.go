package main

import "testing"

func TestDivide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2/2=1",
			args: args{2, 2},
			want: 1,
		}, {
			name: "3/3=1",
			args: args{3, 3},
			want: 1,
		}, {
			name: "100/10=10",
			args: args{100, 10},
			want: 10,
		}, {
			name: "1/2=0.5",
			args: args{1, 2},
			want: 0.5,
		}, {
			name: "0/1=0",
			args: args{0, 1},
			want: 0,
		}, {
			name: "5/4=1.25",
			args: args{5, 4},
			want: 1.25,
		}, {
			name: "10000/1000=10",
			args: args{10000, 1000},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			calc := NewCalc()
			if got := calc.SetA(tt.args.a).SetB(tt.args.b).Do(divide).Result(); got != tt.want {
				t.Errorf("divide = %v, want %v", got, tt.want)
			}
		})
	}

}
