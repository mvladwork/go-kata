package main

import "testing"

func TestMultiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2*2=4",
			args: args{2, 2},
			want: 4,
		}, {
			name: "3*3=9",
			args: args{3, 3},
			want: 9,
		}, {
			name: "10*10=100",
			args: args{10, 10},
			want: 100,
		}, {
			name: "1*1=1",
			args: args{1, 1},
			want: 1,
		}, {
			name: "1*0=0",
			args: args{1, 0},
			want: 0,
		}, {
			name: "1*90=90",
			args: args{1, 90},
			want: 90,
		}, {
			name: "0*0=0",
			args: args{0, 0},
			want: 0,
		}, {
			name: "1000*1000=1000000",
			args: args{1000, 1000},
			want: 1000000,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			calc := NewCalc()
			if got := calc.SetA(tt.args.a).SetB(tt.args.b).Do(multiply).Result(); got != tt.want {
				t.Errorf("multiply = %v, want %v", got, tt.want)
			}
		})
	}

}
