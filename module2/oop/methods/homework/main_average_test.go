package main

import "testing"

func TestAverage(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2,2 = среднее 2",
			args: args{2, 2},
			want: 2,
		}, {
			name: "10,8 = среднее 9",
			args: args{10, 8},
			want: 9,
		}, {
			name: "10,34 = среднее 22",
			args: args{10, 34},
			want: 22,
		}, {
			name: "1,10 = среднее 5,5",
			args: args{1, 10},
			want: 5.5,
		}, {
			name: "1,0 = среднее 0,5",
			args: args{1, 0},
			want: 0.5,
		}, {
			name: "0,0 = среднее 0",
			args: args{0, 0},
			want: 0,
		}, {
			name: "1000,5000=2000",
			args: args{1000, 5000},
			want: 3000,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			calc := NewCalc()
			if got := calc.SetA(tt.args.a).SetB(tt.args.b).Do(average).Result(); got != tt.want {
				t.Errorf("average = %v, want %v", got, tt.want)
			}
		})
	}

}
