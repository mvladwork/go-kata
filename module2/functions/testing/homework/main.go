package main

import (
	"fmt"
	"regexp"
)

func Greet(name string) string {
	re := regexp.MustCompile("[А-Яа-я]+?") //проверяем на киррилические символы
	if re.MatchString(name) {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}

func main() {
	s := "John"
	//s := "Боря"
	fmt.Println(Greet(s))
}
