package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {

	defer func() {
		job.IsFinished = true
	}()

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	job.Merged = make(map[string]string)
	for _, m := range job.Dicts {
		if m == nil {
			job.Merged = nil
			return job, errNilDict
		}
		for k, v := range m {
			job.Merged[k] = v
		}
	}

	return job, nil
}

func main() {
	// Пример работы
	a, b := ExecuteMergeDictsJob(&MergeDictsJob{})
	fmt.Println(a, b)
	//&MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
	//a, b := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
	//&MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
	//a, b := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
	//&MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
}
