package main

import (
	"math/rand"

	"github.com/brianvoe/gofakeit"
)

type User struct {
	ID       int64
	Name     string
	Products []Product
}

type Product struct {
	UserID int64
	Name   string
}

func main() {
	users := genUsers()
	//precho(users)
	products := genProducts()
	//precho(products)
	_ = MapUserProducts(users, products)
	//precho(users)
	_ = MapUserProducts2(users, products)
	//precho(users)
}

func MapUserProducts(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func MapUserProducts2(users []User, products []Product) []User {
	var mproducts = make(map[int64][]Product, 100)
	for _, v := range products {
		mproducts[v.UserID] = append(mproducts[v.UserID], v)
	}
	for i := range users {
		users[i].Products = mproducts[int64(i)]
	}

	return users
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		product.Name = gofakeit.Word()
		product.UserID = int64(rand.Intn(100))
		products[i] = product
	}

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		user.Name = gofakeit.FirstName()
		user.ID = int64(i)
		users[i] = user
	}

	return users
}

//func precho(d interface{}) {
//
//	switch m := d.(type) {
//	case []User:
//		for _, v := range m {
//			fmt.Println(v)
//		}
//	case []Product:
//		for _, v := range m {
//			fmt.Println(v)
//		}
//	case map[int64][]Product:
//		for i, v := range m {
//			fmt.Println(i, v)
//		}
//	}
//}
