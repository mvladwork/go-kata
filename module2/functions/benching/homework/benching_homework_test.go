package main

import "testing"

func BenchmarkSample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		users := genUsers()
		products := genProducts()
		_ = MapUserProducts(users, products)
	}
}

func BenchmarkSample2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		users := genUsers()
		products := genProducts()
		_ = MapUserProducts2(users, products)
	}
}
