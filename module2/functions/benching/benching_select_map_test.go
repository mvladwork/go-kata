package main

import "testing"

// selectXIntMap применяется для выборки X элементов из карты
func selectXIntMap(x int, b *testing.B) {
	// Инициализация Map и вставка X элементов
	testmap := make(map[int]int, x)
	// Сброс таймера после инициализации Map
	for i := 0; i < x; i++ {
		// Вставка значения I в ключ I.
		testmap[i] = i
	}
	// holder нужен для хранения найденного int, нельзя извлекать из карты значение без сохранения результата
	var holder int
	//b.ResetTimer()
	for i := 0; i < x; i++ {
		// Выборка из карты
		holder = testmap[i]
	}
	_ = holder
	//Компилятор не оставит без внимания неиспользованный holder, но с помощью быстрой проверки мы его обхитрим.
	//if holder != 0 {
	//
	//}
}

// BenchmarkSelectIntMap1000000 тестирует скорость выборки 1000000 элементов из карты.
func BenchmarkSelectIntMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(1000000, b)
	}
}

// BenchmarkSelectIntMap100000 тестирует скорость выборки 100000 элементов из карты.
func BenchmarkSelectIntMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(100000, b)
	}
}

// BenchmarkSelectIntMap10000 тестирует скорость выборки 10000 элементов из карты.
func BenchmarkSelectIntMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(10000, b)
	}
}

// BenchmarkSelectIntMap1000 тестирует скорость выборки 1000 элементов из карты.
func BenchmarkSelectIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(1000, b)
	}
}

// BenchmarkSelectIntMap100 тестирует скорость выборки 100 элементов из карты.
func BenchmarkSelectIntMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(100, b)
	}
}
