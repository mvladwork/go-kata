package main

import "testing"

// insertXPreallocIntSlice - для добавления X элементов в []int
func insertXPreallocIntSlice(x int, b *testing.B) {
	// Инициализация Slice и вставка X элементов
	testSlice := make([]int, x)
	// сброс таймера
	//b.ResetTimer()
	for i := 0; i < x; i++ {
		testSlice[i] = i
	}
}

// BenchmarkInsertIntSlicePreAllooc1000000 тестирует скорость вставки 1000000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAllooc1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(1000000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc100000 тестирует скорость вставки 100000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAllooc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(100000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc10000 тестирует скорость вставки 10000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAllooc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(10000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc1000 тестирует скорость вставки 1000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAllooc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(1000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc100 тестирует скорость вставки 100 целых чисел в срез.
func BenchmarkInsertIntSlicePreAllooc100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(100, b)
	}
}
