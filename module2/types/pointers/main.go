package main

import "fmt"

func main() {
	var a = 3
	double(&a)
	fmt.Println(a)
	p := &a
	double(p)
	fmt.Println(a, p == nil)
}

func double(x *int) {
	*x += *x
	//x = nil
}
