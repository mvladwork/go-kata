package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeByte()
}

func typeByte() {
	var b byte = 124
	fmt.Println("size is: ", unsafe.Sizeof(b), "bytes")
}
