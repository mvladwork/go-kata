package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 117384938273
	fmt.Println("size is: ", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")

	var uint8Number uint8 = 1 << 7
	var from8 = int8(uint8Number)
	uint8Number--
	var to8 = int8(uint8Number)
	fmt.Println("int8 min value:", from8, "| int8 max value:", to8, "| size:", unsafe.Sizeof(from8), "bytes")

	var uint16Number uint16 = 1 << 15
	var from16 = int16(uint16Number)
	uint16Number--
	var to16 = int16(uint16Number)
	fmt.Println("int16 min value:", from16, "| int16 max value:", to16, "| size:", unsafe.Sizeof(from16), "bytes")

	var uint32Number uint32 = 1 << 31
	var from32 = int32(uint32Number)
	uint32Number--
	var to32 = int32(uint32Number)
	fmt.Println("int32 min value:", from32, "| int32 max value:", to32, "| size:", unsafe.Sizeof(from32), "bytes")

	var uint64Number uint64 = 1 << 63
	var from64 = int64(uint64Number)
	uint64Number--
	var to64 = int64(uint64Number)
	fmt.Println("int64 min value:", from64, "| int64 max value:", to64, "| size:", unsafe.Sizeof(from64), "bytes")

	fmt.Println("=== END type uint ===")
}
