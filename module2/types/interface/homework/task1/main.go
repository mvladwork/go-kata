package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch i := r.(type) {
	case *int:
		if i == nil {
			fmt.Println("Success!")
		}
	}

}
