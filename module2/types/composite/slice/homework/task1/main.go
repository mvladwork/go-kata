package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)

	s2 := []int{1, 2, 3}
	s2 = Append2(s2)
	fmt.Println(s2)
}

func Append(s *[]int) {
	*s = append(*s, 4)
}

func Append2(s []int) []int {
	return append(s, 4)
}
