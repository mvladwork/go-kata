package main

import "fmt"

type Avatar struct {
	URL  string
	Size int64
}

type Client struct {
	ID   int64
	Name string
	Age  int
	IMG  *Avatar
}

func (c Client) HasAvatar() bool {
	if c.IMG != nil && c.IMG.URL != "" {
		return true
	}
	return false
}

func main() {
	//i := new(int64)
	//_ = i

	client := Client{
		ID:   7,
		Name: "Andrey",
		Age:  20,
		IMG: &Avatar{
			URL: "some_url",
		},
	}

	//client.IMG = new(Avatar)
	//client.IMG = &Avatar{}
	fmt.Println(client.HasAvatar())
	fmt.Printf("%#v\n", client)

	//updateAvatar(&client)
	//fmt.Printf("%#v\n", client)

}

//func updateAvatar(client *Client) {
//	client.IMG.URL = "updated_url"
//}

//func updateClient(client Client)  {
//	client.Name = "Artem"
//}
