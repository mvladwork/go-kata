package main

import (
	"fmt"
	"strings"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/caddyserver/caddy",
			Stars: 46600,
		},
		{
			Name:  "https://github.com/avelino/awesome-go",
			Stars: 99100,
		},
		{
			Name:  "https://github.com/traefik/traefik",
			Stars: 42400,
		},
		{
			Name:  "https://github.com/Dreamacro/clash",
			Stars: 39400,
		},
		{
			Name:  "https://github.com/golang-standards/project-layout",
			Stars: 38700,
		},
		{
			Name:  "https://github.com/rclone/rclone",
			Stars: 37800,
		},
		{
			Name:  "https://github.com/nektos/act",
			Stars: 36900,
		},
		{
			Name:  "https://github.com/go-gorm/gorm",
			Stars: 32000,
		},
		{
			Name:  "https://github.com/spf13/cobra",
			Stars: 31300,
		},
		{
			Name:  "https://github.com/gofiber/fiber",
			Stars: 25500,
		},
		{
			Name:  "https://github.com/mattermost/mattermost-server",
			Stars: 25100,
		},
		{
			Name:  "https://github.com/go-kit/kit",
			Stars: 24800,
		},
	}

	p := make(map[string]Project)
	for _, v := range projects {
		p[strings.Replace(v.Name, "https://github.com/", "", 1)] = v
	}

	for _, v := range p {
		fmt.Println(v)
	}
}
