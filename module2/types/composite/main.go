package main

import (
	"fmt"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Location struct {
	Address string
	City    string
	index   string
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {

	user := User{
		Age:  13,
		Name: "Alexander",
	}

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Пушкина, 1.1.1.1",
			City:    "Уфа",
			index:   "453000",
		},
	}

	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(user2)

	//fmt.Println(wallet)
	//fmt.Println("wallet allocates: ", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
}
