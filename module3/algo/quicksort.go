package main

import (
	"math/rand"
	"time"
)

// time complexity: O(n log n)
// in-place
// unstable
func quickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	rand.Seed(time.Now().UnixNano())
	pivot := arr[rand.Intn(len(arr))] // находим медиану выбирая случайное число

	lowPart := make([]int, 0, len(arr))    // сюда записываем числа меньше числа в медиане
	highPart := make([]int, 0, len(arr))   // сюда записываем числа больше числа в медиане
	middlePart := make([]int, 0, len(arr)) // здесь будут храниться числа равные медиане

	for _, item := range arr {
		switch {
		case item < pivot:
			lowPart = append(lowPart, item) // запись чисел меньше медианы
		case item == pivot:
			middlePart = append(middlePart, item) // запись чисел равные медиане
		case item > pivot:
			highPart = append(highPart, item) // запись чисел больше медианы
		}
	}

	lowPart = quickSort(lowPart)
	highPart = quickSort(highPart)

	// распаковываем среднюю часть после всех чисел
	lowPart = append(lowPart, middlePart...)
	// далее распаковываем оставшуюся часть
	lowPart = append(lowPart, highPart...)

	return lowPart
}

func randomData(s int, f int) []int {
	rand.Seed(time.Now().UnixNano())
	var res []int
	for i := s; i < f; i++ {
		res = append(res, rand.Intn(f-s)+s)
	}
	return res
}

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}

	return data
}
