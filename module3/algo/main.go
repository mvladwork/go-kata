package main

import "fmt"

func main() {

	s := []int{9, 10, 3, 2, 4, 6, 5, 7}
	fmt.Println(s)
	//s = mergeSort(s)
	s = bubbleSort(s)
	fmt.Println(s)

	t := &TreeNode{Val: 20}
	_ = t.Insert(30)
	_ = t.Insert(25)
	_ = t.Insert(21)
	_ = t.Insert(22)
	t.PrintInorder()
	fmt.Println(t.FindMax())
	fmt.Println(t.FindMin())
}
