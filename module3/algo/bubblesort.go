package main

// time complexity: O(n^2)
// in-place
// stable
func bubbleSort(data []int) []int {
	swapped := true // создаем переменную для проверки обмена местами в цикле
	i := 1          // выносим переменную отвечающую за изменение длины сортируемой части массива
	for swapped {
		swapped = false                      // по-умолчанию говорим что обмена не было в цикле
		for j := 0; j < len(data)-i-1; j++ { // len(data)-i не тратим время на отсортированную часть
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true // ставим swapped в true, если значения поменялись местами
			}
		}
		i++ // инкременируем индекс для пропуска отсортированной части
	}
	return data
}
