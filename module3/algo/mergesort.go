package main

// time complexity: O(n log n)
// not in-place
// stable
func mergeSort(s []int) []int {
	if len(s) < 2 {
		return s
	}
	left := mergeSort(s[:len(s)/2])
	right := mergeSort(s[len(s)/2:])
	var res []int
	var i, j int
	// сравниваем элементы до тех пор, пока не будет проверен последний элемент левого или правого слайса
	for i < len(left) && j < len(right) {
		if left[i] < right[j] {
			res = append(res, left[i])
			i++
		} else {
			res = append(res, right[j])
			j++
		}
	}
	// если остались элементы в левом слайсе, добавляем их в результат
	for ; i < len(left); i++ {
		res = append(res, left[i])
	}
	// если остались элементы в правом слайсе, добавляем их в результат
	for ; j < len(right); j++ {
		res = append(res, right[j])
	}
	return res
}
