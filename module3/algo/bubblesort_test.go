package main

import (
	"reflect"
	"testing"
)

func Test_bubbleSort(t *testing.T) {
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed slice",
			args: args{
				data: []int{55, 21, 3, 66},
			},
			want: []int{3, 21, 55, 66},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bubbleSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("bubblesort() = %v, want %v", got, tt.want)
			}
		})
	}
}
