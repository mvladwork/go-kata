package main

import (
	"reflect"
	"sort"
	"testing"
)

func Test_quickSort(t *testing.T) {
	data := randomData(100, 1000)
	sorted := data
	sort.Ints(sorted)
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed slice",
			args: args{
				data: []int{55, 21, 3, 66},
			},
			want: []int{3, 21, 55, 66},
		}, {
			name: "random 1000 numbers",
			args: args{
				data: data,
			},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := quickSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("quickSort() = %v, want %v", got, tt.want)
			}
		})
	}
}
