package main

import "testing"

//func BenchmarkBubbleSortOld(b *testing.B) {
//	data := []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		bubbleSortOld(data)
//	}
//}

func BenchmarkMergeSort(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		mergeSort(dataSet[i])
	}
}
