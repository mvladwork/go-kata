package main

import (
	"fmt"
	"os"

	repository "gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/service"
)

const (
	// PATH путь к бд
	PATH = "module3/clean_architecture/service/todo.json"
)

func main() {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: PATH}})
	fmt.Println(" ToDo")

	iter := 0
	for {

		var input int
		if iter > 0 {
			fmt.Print("\nвыберите действие: ")
			fmt.Scanln(&input)
		} else {
			input = 0
		}
		iter++

		switch input {
		case 0:
			fmt.Println("-------")
			fmt.Println("0: вывести меню")
			fmt.Println("1: показать все задачи")
			fmt.Println("2: добавить новую задачу")
			fmt.Println("3: удалить задачу")
			fmt.Println("4: изменить задачу по ID")
			fmt.Println("5: завершить задачу")
			fmt.Println("6: выход")
		case 1:
			todoList, _ := todo.ListTodos()
			if len(todoList) > 0 {
				for _, v := range todoList {
					fmt.Printf("ID:%v, название:%v, статус:%v\n", v.ID, v.Title, v.Status)
				}
			} else {
				fmt.Println("задачи не найдены")
			}
		case 2:
			fmt.Println("Введите название новой задачи:")
			var title string
			fmt.Scanln(&title)
			err := todo.CreateTodo(title)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("задача успешно добавлена!")
			}
		case 3:
			fmt.Println("введите ID задачи для удаления:")
			var id int
			fmt.Scanln(&id)
			err := todo.RemoveTodo(id)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("задача успешно удалена!")
			}
		case 4:
			fmt.Println("введите ID задачи для изменения:")
			var id int
			fmt.Scanln(&id)

			taskBool, _ := todo.IsTodo(id)
			if !taskBool {
				fmt.Println("Задачи с таким ID нет в базе")
				break
			}

			fmt.Println("введите новое название задачи:")
			var title string
			fmt.Scanln(&title)

			if err := todo.ChangeTodo(repository.Task{Title: title, ID: id}); err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("задача успешно изменена!")
			}

		case 5:
			fmt.Println("введите ID задачи для закрытия:")
			var id int
			fmt.Scanln(&id)
			err := todo.CompleteTodo(id)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("задача успешно закрыта!")
			}
		case 6:
			fmt.Println("выход")
			os.Exit(1)

		default:
			fmt.Println("выберите цифру от 1 до 6:")
		}
	}
}
