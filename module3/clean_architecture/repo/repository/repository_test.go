package repository

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {

	f, err := os.OpenFile("../db_test.json", os.O_RDWR, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	rw := io.ReadWriteSeeker(f)
	repo := NewUserRepository(rw)

	type args struct {
		id int
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			name: "проверка поиска по существующему юзеру",
			args: args{id: 1},
			want: User{ID: 1, Name: "Atos"},
		},
		{
			name: "проверка поиска по несуществующему юзеру",
			args: args{id: -999},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := repo.Find(tt.args.id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {

	f, err := os.OpenFile("../db_test.json", os.O_RDWR, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	rw := io.ReadWriteSeeker(f)
	repo := NewUserRepository(rw)

	tests := []struct {
		name string
		want interface{}
	}{
		{
			name: "вывод всех юзеров",
			want: []interface{}{User{ID: 1, Name: "Atos"}, User{ID: 2, Name: "Portos"}, User{ID: 3, Name: "Aramis"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := repo.FindAll(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() = %v, want %v", got, tt.want)
				return
			}
		})
	}
}
