package repository

import (
	"encoding/json"
	"errors"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Save Загружать данные в конструкторе
func (r *UserRepository) Save(record interface{}) error {

	file, err := io.ReadAll(r.File)
	if err != nil {
		return err
	}

	var users []User
	_ = json.Unmarshal(file, &users)

	for _, uIn := range record.([]User) {
		add := true
		for _, uFile := range users {
			if uIn.ID == uFile.ID {
				add = false
			}
		}
		if add {
			users = append(users, uIn)
		}
	}

	data, err := json.MarshalIndent(users, "", "	")
	if err != nil {
		return err
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return err
	}
	_, err = r.File.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {

	_, err := r.File.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}

	data, err := io.ReadAll(r.File)
	if err != nil {
		return nil, err
	}

	var users []User
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	for _, u := range users {
		if u.ID == id {
			return u, nil
		}
	}

	return nil, errors.New("user not found")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	_, err := r.File.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}

	data, err := io.ReadAll(r.File)
	if err != nil {
		return nil, err
	}

	var users []User
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	records := make([]interface{}, len(users))
	for i, user := range users {
		records[i] = user
	}

	return records, nil
}
