package main

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/mvladwork/go-kata/module3/clean_architecture/repo/repository"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {

	users := []repository.User{
		repository.User{
			ID:   1,
			Name: "Atos",
		},
		repository.User{
			ID:   2,
			Name: "Portos",
		},
	}

	f, err := os.OpenFile("module3/clean_architecture/repo/db.json", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	check(err)
	defer f.Close()
	rw := io.ReadWriteSeeker(f)
	repo := repository.NewUserRepository(rw)
	err = repo.Save(users)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(repo.Find(1))
	fmt.Println(repo.FindAll())

}
