package main

import (
	"fmt"

	repository "gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/repo"

	"gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/service"
)

const (
	// PATH путь к бд
	PATH = "module3/clean_architecture/service/todo.json"
)

func main() {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: PATH}})
	//fmt.Println(todo.ListTodos())
	//fmt.Println(todo.CreateTodo("first task"))
	//fmt.Println(todo.CompleteTodo(2))
	fmt.Println(todo.RemoveTodo(9))
}
