package service

import (
	repository "gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/repo"
)

type TodoServiceIn interface {
	ListTodos() ([]repository.Task, error)
	CreateTodo(title string) error
	CompleteTodo(id int) error
	RemoveTodo(id int) error
	IsTodo(id int) (bool, error)
	ChangeTodo(repository.Task) error
}

type TodoService struct {
	Repository repository.TaskRepository
}

func (s *TodoService) ListTodos() ([]repository.Task, error) {
	//fmt.Println("ListTodos()")
	tasks, err := s.Repository.GetTasks()
	if err != nil {
		return nil, err
	}
	return tasks, nil
}

func (s *TodoService) IsTodo(id int) (bool, error) {
	task, err := s.Repository.GetTask(id)
	if err != nil {
		return false, err
	}
	if task.ID == id {
		return true, nil
	}
	return false, err
}

func (s *TodoService) CreateTodo(title string) error {
	//fmt.Printf("CreateTodo(%v):\n", title)
	task := repository.Task{
		Title:       title,
		Description: "description",
		Status:      "open",
	}
	err := s.Repository.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(id int) error {
	task, _ := s.Repository.GetTask(id)
	task.Status = "closed"
	err := s.Repository.UpdateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) ChangeTodo(task repository.Task) error {
	//fmt.Println("ChangeTodo()")
	chTask, _ := s.Repository.GetTask(task.ID)
	chTask.Title = task.Title
	//fmt.Println(chTask)
	err := s.Repository.UpdateTask(chTask)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(id int) error {
	err := s.Repository.DeleteTask(id)
	if err != nil {
		return err
	}
	return nil
}
