package main

import (
	"reflect"
	"testing"

	repository "gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/repo"
)

func TestTaskRepository_GetTasks(t *testing.T) {

	repo := repository.FileTaskRepository{File: "./todo_get_test.json"}

	tests := []struct {
		name string
		want []repository.Task
	}{
		{
			name: "вывести все таски",
			want: []repository.Task{
				repository.Task{
					ID:          1,
					Title:       "first task",
					Description: "description",
					Status:      "open",
				},
				repository.Task{
					ID:          2,
					Title:       "second task",
					Description: "description",
					Status:      "closed",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := repo.GetTasks(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestTaskRepository_GetTask(t *testing.T) {

	repo := repository.FileTaskRepository{File: "./todo_get_test.json"}

	tests := []struct {
		name string
		want repository.Task
	}{
		{
			name: "вывести таску с ID = 1",
			want: repository.Task{
				ID:          1,
				Title:       "first task",
				Description: "description",
				Status:      "open",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := repo.GetTask(1); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTask = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestTaskRepository_DeleteTask(t *testing.T) {

	repo := repository.FileTaskRepository{File: "./todo_create_test.json"}
	newTask := repository.Task{
		Title:       "deleteTest",
		Description: "description",
		Status:      "open",
	}
	_ = repo.CreateTask(newTask)

	tests := []struct {
		name string
		want error
	}{
		{
			name: "тест на правильность удаления таски",
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := repo.DeleteTask(1); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DeleteTask = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestTaskRepository_CreateTask(t *testing.T) {

	repo := repository.FileTaskRepository{File: "./todo_create_test.json"}
	newTask := repository.Task{
		Title:       "createTest",
		Description: "description",
		Status:      "open",
	}
	_ = repo.CreateTask(newTask)

	task, _ := repo.GetTask(1)

	tests := []struct {
		name string
		want repository.Task
	}{
		{
			name: "тест на правильность создания таски",
			want: repository.Task{
				ID:          1,
				Title:       "createTest",
				Description: "description",
				Status:      "open",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := task; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTask = %v, want %v", got, tt.want)
				return
			}
			_ = repo.DeleteTask(1)
		})
	}
}

func TestTaskRepository_UpdateTask(t *testing.T) {

	repo := repository.FileTaskRepository{File: "./todo_create_test.json"}
	newTask := repository.Task{
		Title:       "test",
		Description: "description",
		Status:      "open",
	}
	_ = repo.CreateTask(newTask)

	newTask.ID = 1
	newTask.Title = "updateTest"

	_ = repo.UpdateTask(newTask)

	testTask, _ := repo.GetTask(1)

	tests := []struct {
		name string
		want string
	}{
		{
			name: "тест на правильность удаления таски",
			want: "updateTest",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := testTask.Title; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask = %v, want %v", got, tt.want)
				return
			}
			_ = repo.DeleteTask(1)
		})
	}
}
