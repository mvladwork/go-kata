package main

import (
	"reflect"
	"testing"

	"gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/service"

	repository "gitlab.com/mvladwork/go-kata/module3/clean_architecture/service/repo"
)

func TestTaskService_ListTodos(t *testing.T) {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: "./todo_get_test.json"}})

	tests := []struct {
		name string
		want []repository.Task
	}{
		{
			name: "вывести все таски",
			want: []repository.Task{
				repository.Task{
					ID:          1,
					Title:       "first task",
					Description: "description",
					Status:      "open",
				},
				repository.Task{
					ID:          2,
					Title:       "second task",
					Description: "description",
					Status:      "closed",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := todo.ListTodos(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestTaskService_RemoveTodo(t *testing.T) {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: "./todo_create_test.json"}})

	_ = todo.CreateTodo("removeTest")

	tests := []struct {
		name string
		want error
	}{
		{
			name: "тест на правильность удаления таски",
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := todo.RemoveTodo(1); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveTodo = %v, want %v", got, tt.want)
				return
			}
		})
	}
}

func TestTaskService_CreateTodo(t *testing.T) {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: "./todo_create_test.json"}})

	tests := []struct {
		name string
		want error
	}{
		{
			name: "тест на правильность создания таски",
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := todo.CreateTodo("createTest"); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTodo = %v, want %v", got, tt.want)
				return
			}
			_ = todo.RemoveTodo(1)
		})
	}
}

func TestTaskService_ChangeTodo(t *testing.T) {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: "./todo_create_test.json"}})
	_ = todo.CreateTodo("test")

	newTask := repository.Task{
		ID:          1,
		Title:       "changeTest",
		Description: "description",
		Status:      "open",
	}

	tests := []struct {
		name string
		want error
	}{
		{
			name: "тест на правильность изменения таски",
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := todo.ChangeTodo(newTask); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ChangeTodo = %v, want %v", got, tt.want)
				return
			}
			_ = todo.RemoveTodo(1)
		})
	}
}

func TestTaskService_CompleteTodo(t *testing.T) {

	todo := service.TodoServiceIn(&service.TodoService{Repository: &repository.FileTaskRepository{File: "./todo_create_test.json"}})
	_ = todo.CreateTodo("completeTest")

	tests := []struct {
		name string
		want error
	}{
		{
			name: "проверить обновление статуса",
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := todo.CompleteTodo(1); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CompleteTodo = %v, want %v", got, tt.want)
				return
			}
			_ = todo.RemoveTodo(1)
		})
	}
}
