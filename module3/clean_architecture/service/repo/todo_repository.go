package repository

import (
	"encoding/json"
	"io"
	"os"
)

type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) error
	UpdateTask(task Task) error
	DeleteTask(id int) error
}

type FileTaskRepository struct {
	File string
}

func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	//fmt.Println("GetTasks()")
	fd, err := os.OpenFile(repo.File, os.O_CREATE|os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	var tasks []Task
	content, err := io.ReadAll(fd)
	if err != nil {
		return nil, err
	}
	_ = json.Unmarshal(content, &tasks)

	return tasks, nil
}

func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	//fmt.Printf("GetTask(%v)\n", id)
	var task Task
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, nil
}

func (repo *FileTaskRepository) CreateTask(task Task) error {
	//fmt.Println("create task:", task)
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	taskID := 0
	for _, v := range tasks {
		if v.ID > taskID {
			taskID = v.ID
		}
	}
	task.ID = taskID + 1
	tasks = append(tasks, task)
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}

	return nil
}

func (repo *FileTaskRepository) saveTasks(tasks []Task) error {
	//fmt.Println("save tasks:", tasks)
	fd, err := os.OpenFile(repo.File, os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer fd.Close()
	data, err := json.MarshalIndent(tasks, "", " ")
	if err != nil {
		return err
	}
	_, err = fd.Seek(0, 0)
	if err != nil {
		return err
	}
	_, err = fd.Write(data)
	if err != nil {
		return err
	}

	return nil
}

// UpdateTask Обновить task
func (repo *FileTaskRepository) UpdateTask(task Task) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	for i, v := range tasks {
		if task.ID == v.ID {
			tasks[i] = task
			break
		}
	}
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}

	return nil
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	for i, v := range tasks {
		if id == v.ID {
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
	}
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}

	return nil
}
