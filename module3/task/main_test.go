package main

import (
	"log"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit"
)

func TestLoadData(t *testing.T) {
	file, err := os.Create("test_data.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	testData := []byte(`[
    {"message":"testMessage1","uuid":"111","date":"1925-06-30T09:27:05.280535537Z"},
    {"message":"testMessage2","uuid":"222","date":"1926-06-30T09:27:05.280535537Z"}
  ]`)
	if _, err := file.Write(testData); err != nil {
		t.Fatal(err)
	}
	d := &DoubleLinkedList{}
	if err := d.LoadData(file.Name()); err != nil {
		t.Errorf("LoadData returned an error: %v", err)
	}
	if d.len != 2 {
		t.Errorf("List length is incorrect. Expected: %d, actual: %d", 2, d.len)
	}
	if d.head.data.Message != "testMessage1" {
		t.Errorf("List head is incorrect. Expected: %s, actual: %s", "testMessage", d.head.data.Message)
	}
	if d.tail.data.UUID != "222" {
		t.Errorf("List tail is incorrect. Expected: %s, actual: %s", "111", d.tail.data.UUID)
	}
	if !d.tail.data.Date.Equal(time.Date(1926, 6, 30, 9, 27, 5, 280535537, time.UTC)) {
		t.Errorf("List tail is incorrect. Expected: %v, actual: %v", time.Date(1926, 6, 30, 9, 27, 5, 280535537, time.UTC), d.tail.data.Date)
	}
	curr := d.head
	for curr != nil && curr.next != nil {
		if curr.data.Date.After(curr.next.data.Date) {
			t.Errorf("List is not sorted. Expected: %v <= %v, actual: %v > %v", curr.data.Date, curr.next.data.Date, curr.data.Date, curr.next.data.Date)
		}
		curr = curr.next
	}
}

func TestLen(t *testing.T) {
	d := &DoubleLinkedList{}
	if d.Len() != 0 {
		t.Errorf("List length is incorrect. Expected: %d, actual: %d", 0, d.Len())
	}
	if err := d.LoadData("test.json"); err != nil {
		log.Println(err)
	}
	if d.Len() == 0 {
		t.Errorf("List length is incorrect")
	}
}

func TestCurrent(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{curr: node}
	if d.Current() != node {
		t.Errorf("Current post is wrong")
	}
}

func TestNext(t *testing.T) {
	headNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	tailNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "2", Date: time.Now()},
		prev: headNode,
		next: nil,
	}
	headNode.next = tailNode
	d := &DoubleLinkedList{curr: headNode}
	if d.Next() != d.curr.next {
		t.Errorf("Next post is wrong")
	}
}

func TestPrev(t *testing.T) {
	headNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	tailNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "2", Date: time.Now()},
		prev: headNode,
		next: nil,
	}
	headNode.next = tailNode
	d := &DoubleLinkedList{curr: tailNode}
	if d.Prev() != d.curr.prev {
		t.Errorf("Next post is wrong")
	}
}

func TestInsert(t *testing.T) {
	d := &DoubleLinkedList{}
	commit := &Commit{
		Message: "testMessage",
		UUID:    "1",
		Date:    time.Now(),
	}
	err := d.Insert(0, *commit)
	if err != nil {
		t.Errorf("deleteCurrent error")
	}
	if *d.head.data != *commit {
		t.Errorf("Insert	 doesn't work")
	}
}

func TestDelete(t *testing.T) {
	headNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: headNode}
	d.len++

	err := d.Delete(0)
	if err != nil {
		t.Errorf("delete error")
	}
	if d.head != nil {
		t.Errorf("Delete doesn't work")
	}
}

func TestDeleteCurrent(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: node, curr: node}
	d.len++

	err := d.DeleteCurrent()
	if err != nil {
		t.Errorf("deleteCurrent error")
	}

	if d.curr != nil {
		t.Errorf("DeleteCurrent doesn't work")
	}
}

func TestIndex(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: node, curr: node}
	d.len++
	index, err := d.Index()
	if err != nil {
		t.Errorf("Index error")
	}
	if index != 0 {
		t.Errorf("Index doesn't work")
	}
}

func TestPop(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{tail: node}
	d.len++

	popNode := d.Pop()
	if popNode != node {
		t.Errorf("Pop doesn't work")
	}
}

func TestShift(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: node}
	d.len++

	shiftNode := d.Shift()
	if shiftNode != node {
		t.Errorf("Pop doesn't work")
	}
}

func TestSearchUUID(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: node}
	d.len++
	if d.SearchUUID("1") != node {
		t.Errorf("SearchUUID doesn't work")
	}
}

func TestSearch(t *testing.T) {
	node := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	d := &DoubleLinkedList{head: node}
	d.len++

	if d.Search("testMessage") != node {
		t.Errorf("Search doesn't work")
	}
}

func TestReverse(t *testing.T) {
	headNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "1", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	tailNode := &Node{
		data: &Commit{Message: "testMessage", UUID: "2", Date: time.Now()},
		prev: headNode,
		next: nil,
	}
	headNode.next = tailNode

	d := &DoubleLinkedList{head: headNode, tail: tailNode}
	oldHead := d.head
	if (d.Reverse().head) == oldHead {
		t.Errorf("Reverse doesn't work")
	}
}

func BenchmarkLoadData(b *testing.B) {
	d := &DoubleLinkedList{}
	for i := 0; i < b.N; i++ {
		if err := d.LoadData("test.json"); err != nil {
			b.Errorf("LoadData returned an error: %v", err)
		}
	}
}

func BenchmarkLen(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.Len()
	}
}

func BenchmarkCurrent(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}
	d.curr = d.head

	for i := 0; i < b.N; i++ {
		d.Current()
	}
}

func BenchmarkNext(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}
	d.curr = d.head
	for i := 0; i < b.N; i++ {
		d.Next()
	}
}

func BenchmarkPrev(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}
	d.curr = d.tail
	for i := 0; i < b.N; i++ {
		d.Next()
	}
}

func BenchmarkInsert(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		commit := &Commit{
			Message: "testMessage",
			UUID:    strconv.Itoa(i),
			Date:    time.Now(),
		}
		if err := d.Insert(1, *commit); err != nil {
			log.Println(err)
		}
	}
}

func BenchmarkDelete(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		_ = d.Delete(0)
	}
}

func BenchmarkDeleteCurrent(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}
	d.curr = d.head
	for i := 0; i < b.N; i++ {
		_ = d.DeleteCurrent()
	}
}

func BenchmarkIndex(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}
	for i := 0; i < b.N; i++ {
		_, _ = d.Index()
	}
}

func BenchmarkPop(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.Pop()
	}
}

func BenchmarkShift(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.Shift()
	}
}

func BenchmarkSearchUUID(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.SearchUUID(gofakeit.UUID())
	}
}

func BenchmarkSearch(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.Search("Try to parse the AGP application, maybe it will index the primary sensor!")
	}
}

func BenchmarkReverse(b *testing.B) {
	d := &DoubleLinkedList{}
	if err := d.LoadData("test.json"); err != nil {
		b.Errorf(err.Error())
	}

	for i := 0; i < b.N; i++ {
		d.Reverse()
	}
}
