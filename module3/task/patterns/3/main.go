package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type City struct {
	name string
	lat  float64
	lon  float64
}
type Result struct {
	Weather Weather `json:"fact"`
}
type Weather struct {
	Temp      float64 `json:"temp"`
	WindSpeed float64 `json:"wind_speed"`
	Humidity  float64 `json:"humidity"`
}

type WeatherAPI interface {
	GetWeatherData(location City) (float64, float64, float64)
}

type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetWeatherData(location City) (float64, float64, float64) {

	url := fmt.Sprint("https://api.weather.yandex.ru/v2/forecast?lat=", location.lat, "&lon=", location.lon, "&extra=true")
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Println(err)
		return 0, 0, 0
	}
	req.Header.Add("X-Yandex-API-Key", o.apiKey)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return 0, 0, 0
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		fmt.Println("wrong status code: ", res.StatusCode)
		return 0, 0, 0
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return 0, 0, 0
	}
	result := Result{}
	if err := json.Unmarshal(body, &result); err != nil {
		fmt.Println(err)
		return 0, 0, 0
	}

	return result.Weather.Temp, result.Weather.Humidity, result.Weather.WindSpeed
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location City) (float64, float64, float64) {

	return w.weatherAPI.GetWeatherData(location)
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("9218d4f6-5e03-4217-b2ca-2c6113cd01dd")
	cities := []City{
		City{
			"Moscow",
			55.75396,
			37.620393,
		},
		City{
			"Saint Petersburg",
			59.9342802,
			30.3350986,
		},
		City{
			"Ufa",
			54.7387621,
			55.9720554,
		},
	}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city.name+": %v\n", temperature)
		fmt.Printf("Humidity in "+city.name+": %v\n", humidity)
		fmt.Printf("Wind speed in "+city.name+": %v\n\n", windSpeed)
	}
}
