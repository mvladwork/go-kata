package main

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type RealAirConditioner struct {
	model string
}

func (r *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}
func (r *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}
func (r *RealAirConditioner) SetTemperature(temp int) {
	fmt.Println("Setting air conditioner temperature to", temp)
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}
func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}
func (a *AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (a *AirConditionerProxy) TurnOn() {
	if a.authenticated {
		a.adapter.TurnOn()
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}
func (a *AirConditionerProxy) TurnOff() {
	if a.authenticated {
		a.adapter.TurnOff()
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}
}
func (a *AirConditionerProxy) SetTemperature(temp int) {
	if a.authenticated {
		a.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied: authentication required to set temperature of the air conditioner")
	}
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	realAC := &RealAirConditioner{model: "samsung"}
	adapter := &AirConditionerAdapter{airConditioner: realAC}
	return &AirConditionerProxy{
		adapter:       adapter,
		authenticated: authenticated,
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
