package main

import "fmt"

type PricingStrategy interface {
	Calculate(Order) float64
}

type Order struct {
	name string
	cost float64
	num  int
}

type RegularPricing struct {
}

func (r *RegularPricing) Calculate(order Order) float64 {
	return order.cost
}

type SalePricing struct {
	name     string
	discount float64
}

func (s *SalePricing) Calculate(order Order) float64 {
	return order.cost * (1 - s.discount/100)
}

func main() {
	order := Order{
		name: "iphone",
		cost: 100000,
		num:  1,
	}
	ps := []PricingStrategy{
		&RegularPricing{},
		&SalePricing{
			name:     "personal discount",
			discount: 10,
		},
		&SalePricing{
			name:     "summer discount",
			discount: 5,
		},
	}

	for _, s := range ps {
		fmt.Printf("Total cost with %T strategy: %.2f\n", s, s.Calculate(order))
	}
}
