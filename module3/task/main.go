package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // Начальный элемент в списке
	tail *Node // Последний элемент в списке
	curr *Node // Текущий элемент меняется при использовании методов next, prev
	len  int   // Количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(string) error
	Insert(int, Commit) error
	Delete(int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(string) *Node
	Search(string) *Node
	Reverse() *DoubleLinkedList
	Inspect()
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	f, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	var data []Commit
	if err := json.Unmarshal(f, &data); err != nil {
		return err
	}

	data = QuickSort(data)
	//for _, v := range data {
	//	fmt.Println(v.Date)
	//}

	for i, commit := range data {
		committ := commit
		node := &Node{
			data: &committ,
			prev: nil,
			next: nil,
		}
		if i == 0 {
			d.head = node
			d.tail = node
		} else {
			node.prev = d.tail
			d.tail.next = node
			d.tail = node
		}
		d.len++
	}

	return err
}

func QuickSort(commits []Commit) []Commit {
	if len(commits) < 2 {
		return commits
	}
	rand.Seed(time.Now().UnixNano())
	pivot := commits[rand.Intn(len(commits))]

	var left []Commit
	var middle []Commit
	var right []Commit
	for _, v := range commits {
		switch {
		case v.Date.Before(pivot.Date):
			left = append(left, v)
		case v.Date.Equal(pivot.Date):
			middle = append(middle, v)
		case v.Date.After(pivot.Date):
			right = append(right, v)
		}
	}
	left = QuickSort(left)
	right = QuickSort(right)

	return append(append(left, middle...), right...)
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n > d.len {
		return errors.New("n-ого элемента не существует в списке")
	}
	node := &Node{
		data: &c,
		prev: nil,
		next: nil,
	}
	if n == 0 {
		d.head = node
		d.tail = node
	} else if n == d.len {
		node.prev = d.head
		d.tail.next = node
		d.tail = node
	} else {
		currentPost := d.head
		for i := 0; i < n; i++ {
			currentPost = currentPost.next
		}
		node.prev = currentPost
		node.next = currentPost.next
		currentPost.next = node
		currentPost.next.prev = node
	}
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n > d.len {
		return errors.New("n-ого элемента не существует в списке")
	}
	if d.len == 0 {
		return errors.New("пустой список")
	}
	if d.len == 1 {
		d.head = nil
		d.tail = nil
		d.curr = nil
	} else if n == d.len-1 {
		d.tail = d.tail.prev
		d.tail.next = nil
	} else if n == 0 {
		d.head = d.head.next
		d.head.prev = nil
	} else {
		currNode := d.head
		for i := 0; i < n; i++ {
			currNode = currNode.next
		}
		currNode.prev.next = currNode.next
		currNode.next.prev = currNode.prev
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {

	if d.curr == nil {
		return errors.New("текущий элемент не существует")
	}

	currNode := d.head
	for i := 0; i < d.len; i++ {
		if currNode == d.curr {
			err := d.Delete(i)
			if err != nil {
				return err
			}
			return nil
		}
		currNode = currNode.next
	}
	return errors.New("нет текущего элемента")
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return 0, errors.New("текущий элемент nil")
	}
	currNode := d.head
	for i := 0; i < d.len; i++ {
		if currNode == d.curr {
			return i, nil
		}
		currNode = currNode.next
	}
	return 0, errors.New("текущий элемент не найден")
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	}
	popNode := d.tail
	if d.len == 1 {
		d.tail = nil
	} else {
		d.tail.prev.next = nil
		d.tail = d.tail.prev
	}
	d.len--
	return popNode
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		return nil
	}

	shiftNode := d.head

	if d.len == 1 {
		d.head = nil
		d.tail = nil
	} else {
		d.head = d.head.next
		d.head.prev = nil
	}
	d.len--

	return shiftNode
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d.len == 0 {
		return nil
	}
	currentPost := d.head
	for i := 0; i < d.len; i++ {
		if currentPost.data.UUID == uuID {
			return currentPost
		}
		currentPost = currentPost.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.len == 0 {
		log.Println("список пуст")
	}
	currNode := d.head
	for i := 0; i < d.len; i++ {
		if currNode.data.Message == message {
			return currNode
		}
		currNode = currNode.next

	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	currNode := d.head
	var nextInList *Node
	d.head, d.tail = d.tail, d.head
	for currNode != nil {
		nextInList = currNode.next
		currNode.next, currNode.prev = currNode.prev, currNode.next
		currNode = nextInList
	}
	return d
}

func (d *DoubleLinkedList) Inspect() {
	if d.len == 0 {
		fmt.Println("список пустой")
		return
	}
	currNode := d.head
	fmt.Printf("////// Inspect //////\nFeed lenght: %v\nFirst: %v\nEnd: %v\n", d.len, d.head.data, d.tail.data)

	for i := 1; i <= d.len; i++ {
		fmt.Printf("%v - %v\n", i, currNode.data)
		currNode = currNode.next
	}
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func main() {
	dllist := DoubleLinkedList{}
	if err := dllist.LoadData("module3/task/test.json"); err != nil {
		log.Println(err)
	}
	dllist.Inspect()
}
