package main

import "fmt"

type Client struct {
}

// InsertLightningConnectorIntoComputer
// Клиент ожидает, что объект реализует интерфейс Computer (InsertIntoLightningPort)
func (c *Client) InsertLightningConnectorIntoComputer(com Computer) {
	fmt.Println("Клиент вставил Lightning в компьютер")
	com.InsertIntoLightningPort()
}

type Mac struct {
}

func (m *Mac) InsertIntoLightningPort() {
	fmt.Println("Lightning вставлен в mac комп")
}

type Windows struct {
}

func (w *Windows) InsertIntoUsbPort() {
	fmt.Println("Usb вставлен в win комп")
}

type WindowsAdapter struct {
	windowMachine *Windows
}

func (w *WindowsAdapter) InsertIntoLightningPort() {
	fmt.Println("Адаптер сконвертировал Lightning в Usb")
	w.windowMachine.InsertIntoUsbPort()
}

type Computer interface {
	InsertIntoLightningPort()
}

func main() {
	client := &Client{}
	mac := &Mac{}
	client.InsertLightningConnectorIntoComputer(mac)
	fmt.Println("---")
	windowsMachine := &Windows{}
	windowsMachineAdapter := &WindowsAdapter{
		windowMachine: windowsMachine,
	}

	client.InsertLightningConnectorIntoComputer(windowsMachineAdapter)
}
