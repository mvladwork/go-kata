package main

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	i, v := helper(nums)
	return &TreeNode{
		Val:   v,
		Left:  constructMaximumBinaryTree(nums[:i]),
		Right: constructMaximumBinaryTree(nums[i+1:]),
	}
}

func helper(nums []int) (index int, value int) {
	if len(nums) == 1 {
		return 0, nums[0]
	}
	for i, v := range nums {
		if v > value {
			value = v
			index = i
		}
	}

	return
}
