package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	var result, last *ListNode
	for head != nil {
		var sum int
		if head.Val != 0 {
			for head.Val != 0 {
				sum += head.Val
				head = head.Next
			}
			newResult := &ListNode{Val: sum}
			if result == nil {
				result = newResult
			} else {
				last.Next = newResult
			}
			last = newResult
		} else {
			head = head.Next
		}
	}
	return result
}
