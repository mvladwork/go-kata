package main

func balanceBST(root *TreeNode) *TreeNode {
	values := inorderTraversal(root)
	return buildBST(values, 0, len(values)-1)
}

func inorderTraversal(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	left := inorderTraversal(root.Left)
	right := inorderTraversal(root.Right)

	return append(append(left, root.Val), right...)
}

func buildBST(values []int, start, end int) *TreeNode {
	if start > end {
		return nil
	}
	mid := (start + end) / 2
	node := &TreeNode{Val: values[mid]}

	node.Left = buildBST(values, start, mid-1)
	node.Right = buildBST(values, mid+1, end)

	return node
}
