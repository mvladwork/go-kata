package main

func deepestLeavesSum(root *TreeNode) int {
	if root == nil {
		return 0
	}

	queue := []*TreeNode{root}
	sum := 0
	for len(queue) > 0 {
		nodesHeight := len(queue)
		sum = 0
		for i := 0; i < nodesHeight; i++ {
			curr := queue[0]
			queue = queue[1:]
			sum += curr.Val
			if curr.Left != nil {
				queue = append(queue, curr.Left)

			}
			if curr.Right != nil {
				queue = append(queue, curr.Right)
			}
		}
	}

	return sum
}
