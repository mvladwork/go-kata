package main

func maxSum(grid [][]int) int {
	var sum, sumMax int
	for i := 1; i < len(grid)-1; i++ {
		for j := 1; j < len(grid[0])-1; j++ {
			sum = grid[i-1][j-1] +
				grid[i-1][j] + grid[i-1][j+1] + grid[i][j] +
				grid[i+1][j+1] + grid[i+1][j] + grid[i+1][j-1]
			if sum > sumMax {
				sumMax = sum
			}
		}
	}

	return sumMax
}
