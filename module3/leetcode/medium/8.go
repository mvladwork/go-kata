package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	vertices := make([]int, n)
	for _, edge := range edges {
		vertices[edge[1]]++
	}
	result := make([]int, 0, n)
	for i, ingoing := range vertices {
		if ingoing == 0 {
			result = append(result, i)
		}
	}

	return result
}
