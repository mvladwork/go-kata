package main

import "fmt"

func pairSum(head *ListNode) int {
	var nums []int
	curr := head
	for curr != nil {
		nums = append(nums, curr.Val)
		curr = curr.Next
	}
	var max int
	for i := 0; i < len(nums)/2; i++ {
		maxSum := nums[i] + nums[len(nums)-i-1]
		fmt.Println(maxSum)
		if maxSum > max {
			max = maxSum
		}
	}
	return max
}
