package main

func numTilePossibilities(tiles string) int {
	sequences := make(map[string]bool)
	var generateSequences func(tiles string, sequence string)
	generateSequences = func(tiles string, sequence string) {
		if len(sequence) > 0 {
			sequences[sequence] = true
		}
		for i := 0; i < len(tiles); i++ {
			nextTiles := tiles[:i] + tiles[i+1:]
			generateSequences(nextTiles, sequence+string(tiles[i]))
		}
	}
	generateSequences(tiles, "")
	return len(sequences)
}
