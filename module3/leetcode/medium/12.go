package main

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	currNode := list1
	for i := 0; i < a-1; i++ {
		currNode = currNode.Next
	}
	insert := currNode

	for i := 0; i < b-a+1; i++ {
		currNode = currNode.Next
	}

	insert.Next = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = currNode.Next
	return list1
}
