package main

func minPartitions(n string) int {
	max := 0
	for _, v := range n {
		tmp := int(v) - 48
		if tmp > max {
			max = tmp
		}
	}

	return max
}
