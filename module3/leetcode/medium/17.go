package main

func countPoints(points [][]int, queries [][]int) []int {
	res := make([]int, len(queries))
	for i, query := range queries {
		for _, point := range points {
			if ((point[0]-query[0])*(point[0]-query[0]))+((point[1]-query[1])*(point[1]-query[1])) <= query[2]*query[2] {
				res[i]++
			}
		}
	}

	return res
}
