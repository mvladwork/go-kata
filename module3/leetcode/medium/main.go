package main

import (
	"errors"
	"fmt"
)

// TreeNode структура бинарного дерева
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	t := &TreeNode{Val: 15}
	_ = t.Insert(12)
	_ = t.Insert(20)
	_ = t.Insert(8)
	_ = t.Insert(13)
	_ = t.Insert(14)
	t.PrintInOrder()
	fmt.Println("deepestLeavesSum = ", deepestLeavesSum(t))

	_ = sortTheStudents(nil, 1)
	_ = mergeNodes(nil)
	_ = bstToGst(nil)
	_ = pairSum(nil)
	_ = constructMaximumBinaryTree(nil)
	_ = balanceBST(nil)
	_ = findSmallestSetOfVertices(1, nil)
	_ = checkArithmeticSubarrays(nil, nil, nil)
	_ = numTilePossibilities("dw ewf")
	_ = removeLeafNodes(nil, 1)
	_ = mergeInBetween(nil, 1, 1, nil)
	_ = maxSum(nil)
	_ = xorQueries(nil, nil)
	_ = minPartitions("")
	_ = countPoints(nil, nil)
}

// PrintInOrder вывести все элементы
func (t *TreeNode) PrintInOrder() {
	if t == nil {
		return
	}
	t.Left.PrintInOrder()
	fmt.Println(t.Val)
	t.Right.PrintInOrder()
}

// Insert вставка нового элемента в дерево
func (t *TreeNode) Insert(value int) error {
	if t == nil {
		return errors.New("дерево имеет значение nil")
	}
	if t.Val == value {
		return errors.New("такое значение в дереве уже существует")
	}
	if t.Val > value {
		if t.Left == nil {
			t.Left = &TreeNode{Val: value}
			return nil
		}
		return t.Left.Insert(value)
	}
	if t.Val < value {
		if t.Right == nil {
			t.Right = &TreeNode{Val: value}
			return nil
		}
		return t.Right.Insert(value)
	}
	return nil
}

// Find поиск элемента со значением value в дереве
func (t *TreeNode) Find(value int) (TreeNode, bool) {
	if t == nil {
		return TreeNode{}, false
	}
	if value == t.Val {
		return *t, true
	} else if value < t.Val {
		return t.Left.Find(value)
	} else {
		return t.Right.Find(value)
	}
}

// Delete удалить элемент из дерева
func (t *TreeNode) Delete(value int) {
	t.remove(value)
}

func (t *TreeNode) remove(value int) *TreeNode {
	if t == nil {
		return nil
	}
	// ищем ноду со значением value
	if value < t.Val {
		t.Left = t.Left.remove(value)
		return t
	}
	if value > t.Val {
		t.Right = t.Right.remove(value)
		return t
	}
	// нода для удаления нашлась, теперь удаляем ее
	// если нода является листом, то удаляем саму ноду, у родителя ветка поменяется
	// на nil (t.Left = t.Left.remove(value))
	if t.Left == nil && t.Right == nil {
		//goland:noinspection ALL
		t = nil
		return nil
	}
	// если у ноды есть только правая ветка, то перезаписываем текущую ноду на правую
	if t.Left == nil {
		//goland:noinspection ALL
		t = t.Right
		return t
	}
	// если у ноды есть только левая ветка, то перезаписываем текущую ноду на левую
	if t.Right == nil {
		//goland:noinspection ALL
		t = t.Left
		return t
	}
	// если ничего вышеперечисленное не сработало, значит у удаляемой ноды две ветки
	// в таком случае находим наименьшее значение в правой ветке и перезаписываем
	// текущую ноду на нее
	min := t.Right
	for {
		if min != nil && min.Left != nil {
			min = min.Left
		} else {
			break
		}
	}
	// записываем значение min как текущее значение и удаляем в правой ветке ноду
	// с min значением
	t.Val = min.Val
	t.Right = t.Right.remove(t.Val)
	return t
}

// FindMax поиск максимального элемента
func (t *TreeNode) FindMax() int {
	if t.Right == nil {
		return t.Val
	}
	return t.Right.FindMax()
}

// FindMin поиск минимального элемента
func (t *TreeNode) FindMin() int {
	if t.Left == nil {
		return t.Val
	}
	return t.Left.FindMin()
}
