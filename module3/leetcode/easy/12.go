package main

func numIdenticalPairs(nums []int) int {
	good := 0
	for i1 := 0; i1 < len(nums)-1; i1++ {
		for i2 := i1 + 1; i2 < len(nums); i2++ {
			if nums[i1] == nums[i2] {
				good++
			}
		}
	}
	return good
}
