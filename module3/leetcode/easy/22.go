package main

func smallerNumbersThanCurrent(nums []int) []int {
	r := make([]int, 0)
	for _, v1 := range nums {
		count := 0
		for _, v2 := range nums {
			if v1 > v2 {
				count++
			}
		}
		r = append(r, count)
	}
	return r
}
