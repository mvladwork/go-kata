package main

func shuffle(nums []int, n int) []int {
	newarr := make([]int, 0)
	for i := 0; i < n; i++ {
		newarr = append(newarr, nums[i], nums[i+n])
	}
	return newarr
}
