package main

func subtractProductAndSum(n int) int {
	c := make([]int, 0)
	for {
		c = append(c, n%10)
		n /= 10
		if n == 0 {
			break
		}
	}
	sum := 0
	multiply := 1
	for _, v := range c {
		sum += v
		multiply *= v
	}
	return multiply - sum
}
