package main

func smallestEvenMultiple(n int) int {
	s := n
	for {
		if s%2 == 0 && s%n == 0 {
			return s
		}
		s++
	}
}
