package main

func numberOfMatches(n int) int {
	if n < 1 || n > 200 {
		panic("error")
	}
	i := 0
	for {
		if n%2 == 0 {
			n = n / 2
			i += n
		} else {
			n = n/2 + 1
			i += n - 1
		}
		if n == 1 {
			break
		}
	}
	return i
}
