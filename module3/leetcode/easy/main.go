package main

import "fmt"

func main() {
	//24
	encoded := []int{6, 2, 7, 3}
	first := 4
	fmt.Println("24. decode =", decode(encoded, first))
	//23
	command := "(al)G(al)()()G"
	fmt.Println("23. interpret =", interpret(command))
	//22
	nums22 := []int{6, 5, 4, 8}
	fmt.Println("22. smallerNumbersThanCurrent =", smallerNumbersThanCurrent(nums22))
	//21
	n21 := 4421
	fmt.Println("21. subtractProductAndSum =", subtractProductAndSum(n21))
	//20
	candies := []int{2, 3, 5, 1, 3}
	extraCandies := 3
	fmt.Println("20. kidsWithCandies =", kidsWithCandies(candies, extraCandies))
	//19
	n19 := 2932
	fmt.Println("19. minimumSum =", minimumSum(n19))
	//18
	nums18 := []int{1, 15, 6, 3}
	fmt.Println("18. differenceOfSum =", differenceOfSum(nums18))
	//17
	sentences := []string{"please wait", "continue to fight", "continue to win"}
	fmt.Println("17. mostWordsFound =", mostWordsFound(sentences))
	//16
	fmt.Println("16. smallestEvenMultiple =", smallestEvenMultiple(6))
	//15
	obj := Constructor(3, 2, 0)
	obj.AddCar(1)
	obj.AddCar(2)
	obj.AddCar(3)
	obj.AddCar(1)
	//14
	accounts := [][]int{{1, 5}, {7, 3}, {3, 5}}
	fmt.Println("14. maximumWealth =", maximumWealth(accounts))
	//13
	jewels := "aA"
	stones := "aAAbbbb"
	fmt.Println("13. numJewelsInStones =", numJewelsInStones(jewels, stones))
	//12
	nums12 := []int{1, 1, 1, 1}
	fmt.Println("12. numIdenticalPairs =", numIdenticalPairs(nums12))
	//11
	nums11 := []int{1, 1, 1, 1, 1}
	fmt.Println("11. runningSum =", runningSum(nums11))
	//10
	nums10 := []int{1, 2, 3, 4, 4, 3, 2, 1}
	n10 := 4
	fmt.Println("10. shuffle =", shuffle(nums10, n10))
	//9
	operations := []string{"X++", "++X", "--X", "X--"}
	fmt.Println("9. finalValueAfterOperations =", finalValueAfterOperations(operations))
	//8
	arr := []int{1, 2, 3, 4}
	k := 2
	fmt.Println("8. findKthPositive =", findKthPositive(arr, k))
	//7
	address := "192.168.1.1"
	fmt.Println("7. defangIPaddr =", defangIPaddr(address))
	//6
	words := []string{"gin", "zen", "gig", "msg"}
	fmt.Println("6. uniqueMorseRepresentations =", uniqueMorseRepresentations(words))
	//5
	fmt.Println("5. numberOfMatches =", numberOfMatches(14))
	//4
	nums4 := []int{0, 2, 1, 5, 3, 4}
	fmt.Println("4. buildArray = ", buildArray(nums4))
	//3
	celsius := 36.50
	fmt.Printf("3. convertTemperature = %.4f\n", convertTemperature(celsius))
	//2
	nums2 := []int{1, 3, 2, 1}
	fmt.Println("2. getConcatenation =", getConcatenation(nums2))
	//1
	fmt.Println("1. tribonacci =", tribonacci(25))
}
