package main

import "math"

func differenceOfSum(nums []int) int {
	sum1 := 0
	sum2 := 0
	for _, v := range nums {
		sum1 += v
		for {
			if v > 9 {
				sum2 += v % 10
				v = v / 10
			} else {
				sum2 += v
				break
			}
		}
	}
	return int(math.Abs(float64(sum2 - sum1)))
}
