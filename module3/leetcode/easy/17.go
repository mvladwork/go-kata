package main

import (
	"strings"
)

func mostWordsFound(sentences []string) int {
	max := 0
	c := 0
	for _, v := range sentences {
		c = len(strings.Split(v, " "))
		if c > max {
			max = c
		}
	}
	return max
}
