package main

import (
	"sort"
)

func minimumSum(num int) int {
	c := make([]int, 4)
	for i := 0; i < 4; i++ {
		c[i] = num % 10
		num /= 10
	}
	sort.Ints(c)
	return (c[0]*10 + c[2]) + (c[1]*10 + c[3])
}
