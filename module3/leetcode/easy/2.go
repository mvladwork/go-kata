package main

func getConcatenation(nums []int) []int {
	n := len(nums)
	if n < 1 || n > 1000 {
		panic("error")
	}
	na := make([]int, n)
	copy(na, nums)
	for _, v := range nums {
		if v < 1 || v > 1000 {
			panic("error")
		}
		na = append(na, v)
	}

	return na
}
