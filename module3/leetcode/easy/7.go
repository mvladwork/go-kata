package main

import (
	"regexp"
	"strings"
)

func defangIPaddr(address string) string {
	p := "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"
	matched, _ := regexp.MatchString("^"+p+`(\.`+p+`){3}$`, address)
	if !matched {
		panic("invalid ip-address")
	}
	return strings.ReplaceAll(address, ".", "[.]")
}
