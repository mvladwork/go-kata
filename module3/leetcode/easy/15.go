package main

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
}

func (p *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1:
		if p.big > 0 {
			p.big -= 1
			return true
		} else {
			return false
		}
	case 2:
		if p.medium > 0 {
			p.medium -= 1
			return true
		} else {
			return false
		}
	case 3:
		if p.small > 0 {
			p.small -= 1
			return true
		} else {
			return false
		}
	default:
		return false
	}
}
