package main

func maximumWealth(accounts [][]int) int {
	res := 0
	for _, v := range accounts {
		sum := 0
		for _, v1 := range v {
			sum += v1
		}
		if sum > res {
			res = sum
		}
	}
	return res
}
