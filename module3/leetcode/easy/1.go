package main

func tribonacci(n int) int {
	if n < 0 || n > 37 {
		panic("Число должно быть от 0 до 37")
	}
	a := map[int]int{}
	for i := 0; i <= n; i++ {
		switch i {
		case 0:
			a[0] = 0
			continue
		case 1:
			a[1] = 1
			continue
		case 2:
			a[2] = 1
			continue
		}
		a[i] = a[i-1] + a[i-2] + a[i-3]
	}
	return a[n]
}
