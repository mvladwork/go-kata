package main

func findKthPositive(arr []int, k int) int {
	count := 1
	i := 0
	res := 0
	arrIndex := 0
	arrLen := len(arr)
	for {
		if i == k {
			return res
		}
		if arrIndex < arrLen {
			if arr[arrIndex] == count {
				arrIndex++
				count++
			} else {
				res = count
				i++
				count++
			}
		} else {
			res = count
			i++
			count++
		}
	}
}
