package main

func runningSum(nums []int) []int {
	sum := 0
	newarr := make([]int, 0)
	for _, v := range nums {
		sum += v
		newarr = append(newarr, sum)
	}
	return newarr
}
