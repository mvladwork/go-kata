package main

func buildArray(nums []int) []int {
	newnums := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		newnums[i] = nums[nums[i]]
	}
	return newnums
}
