package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	res := make([]bool, 0)
	max := 0
	for _, v := range candies {
		if v > max {
			max = v
		}
	}
	for _, v := range candies {
		if v+extraCandies >= max {
			res = append(res, true)
		} else {
			res = append(res, false)
		}
	}
	return res
}
