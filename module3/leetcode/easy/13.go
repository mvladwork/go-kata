package main

func numJewelsInStones(jewels string, stones string) int {
	jcount := 0
	for _, v1 := range stones {
		for _, v2 := range jewels {
			if v1 == v2 {
				jcount++
			}
		}
	}
	return jcount
}
