package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

// Append
// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

// Remove
// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("feed is empty"))
	}
	var previousPost *Post
	currentPost := f.start
	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("no such Post found"))
		}
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next
	f.length--
}

// Insert
// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

// Inspect
// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {

	if f.length == 0 {
		fmt.Println("Feed is empty")
		return
	}

	currentPost := f.start
	fmt.Printf("////// Inspect //////\nFeed lenght: %v\nFirst: %v\nEnd: %v\n", f.length, f.start, f.end)

	for i := 1; i <= f.length; i++ {
		fmt.Printf("%v - %v\n", i, currentPost)
		currentPost = currentPost.next
	}
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}
